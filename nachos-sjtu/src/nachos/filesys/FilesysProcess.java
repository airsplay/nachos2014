package nachos.filesys;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
	protected static final int SYSCALL_MKDIR = 14;
	protected static final int SYSCALL_RMDIR = 15;
	protected static final int SYSCALL_CHDIR = 16;
	protected static final int SYSCALL_GETCWD = 17;
	protected static final int SYSCALL_READDIR = 18;
	protected static final int SYSCALL_STAT = 19;
	protected static final int SYSCALL_LINK = 20;
	protected static final int SYSCALL_SYMLINK = 21;

	public Folder currentFolder;
	
	public RealFileSystem fs;
	public FilesysProcess() {
		super();
		fs = FilesysKernel.realFileSystem;
		currentFolder = fs.getRootFolder();
	}


	@Override
	public File openFile (String name, boolean create) {
		//System.out.println("openFile");
		return fs.open(currentFolder, name, create);	
	}

	@Override
	public boolean removeFile (String name) {
		return fs.remove(currentFolder, name);
	}

	public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
	{
		Tool.PR("a syscall occur:"+syscall);
		switch (syscall)
		{
			case SYSCALL_MKDIR:
				{
					String folderNameWithPath = readVirtualMemoryString(a0, FileNameMaxLen);
					
					return fs.createFolder(currentFolder, folderNameWithPath);
				}

			case SYSCALL_RMDIR:
				{
					String folderNameWithPath = readVirtualMemoryString(a0, FileNameMaxLen);
					int returnValue = fs.removeFolder(currentFolder, folderNameWithPath); 
					Tool.DBG(""+returnValue);
					currentFolder.print("remove");
					return returnValue;
				}

			case SYSCALL_CHDIR:
				{
					String folderNameWithPath = readVirtualMemoryString(a0, FileNameMaxLen);
					Folder tmpFolder =  fs.changeCurrentFolder(currentFolder, folderNameWithPath);
					if (tmpFolder == null) {
						return -1;
					}
					else {
						currentFolder = tmpFolder;
						return 0;
					}
				}

			case SYSCALL_GETCWD:
				{
					String pathString = fs.getPathString(currentFolder);
					int limit = readVirtualMemoryInt(a1);
					//Tool.PR("path:"+pathString + " limit: " + limit);
					if (limit < pathString.length() + 1) {
						return -1;
					}
					else {
						writeVirtualMemoryString(a0, pathString);
						return pathString.length();
					}
				}

			case SYSCALL_READDIR:
				{
					return -1;
				}

			case SYSCALL_STAT:
				{
					return -1;
				}

			case SYSCALL_LINK:
				{
					String srcNameWithPath = readVirtualMemoryString(a0, FileNameMaxLen);
					String dstNameWithPath = readVirtualMemoryString(a1, FileNameMaxLen);
					return fs.createLink(currentFolder, srcNameWithPath, dstNameWithPath);
				}

			case SYSCALL_SYMLINK:
				{
					String srcNameWithPath = readVirtualMemoryString(a0, FileNameMaxLen);
					String dstNameWithPath = readVirtualMemoryString(a1, FileNameMaxLen);
					return fs.createSymLink(currentFolder, srcNameWithPath, dstNameWithPath);
				}

			default:
				return super.handleSyscall(syscall, a0, a1, a2, a3);
		}
	}

	/*public void handleException (int cause)
	  {
	  if (cause == Processor.exceptionSyscall)
	  {
	//TODO implement this
	  }
	  else
	  super.handleException(cause);
	  }*/
}

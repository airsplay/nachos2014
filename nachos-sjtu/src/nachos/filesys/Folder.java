package nachos.filesys;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
	/** the static address for root folder */
	public static int STATIC_ADDR = 1;

	//private int size; // I use entryMap.size() to replace it 
	

	/** mapping from filename to folder entry */
	//public int parentinode; //extract from entryMap.get("..");
	public String folderName;
	private HashMap<String, FolderEntry> entryMap;
//	private LinkedList<FolderEntry> entryList;

	
	public Folder (Inode inode, RealFileSystem fs) {
		super(inode, "", fs);
		this.folderName = "";
		entryMap = new HashMap<String, FolderEntry>();
	}

	public Folder (Inode inode, String folderName, RealFileSystem fs) {
		super(inode, folderName, fs);
		this.folderName = folderName;
		entryMap = new HashMap<String, FolderEntry>();
	}

	public void init () {
		entryMap.clear();
		createEntry(".", this.inode); 
		save();
	}

	public String getName() {
		return folderName;
	}
	public int size() {
		return entryMap.size();
	}

	/** a folder is empty if and only if it has '.' and '..' */
	public boolean isEmpty () {
		Tool.assertOccur(size() == 1 || size() == 0, "unset . or .. in folder " + getName());
		return size() == 2;
	}

	public void clear() {
		//removeEntry(".");
		removeEntry(".."); 
		entryMap.remove(".");
		Tool.assertOccur(!entryMap.isEmpty(), "clear a folder which is not empty. folder " + getName());
	}
	/** set the parent of this folder */
	public void setParent(Folder parentFolder) {
		createEntry("..", parentFolder.inode);
	}
	

	public FolderEntry getFolderEntry(String fileName) {
		FolderEntry entry = entryMap.get(fileName);
		if (entry.inode == null) {
			return null;
		}
		else return entry;
	}

	public FolderEntry getParentFolderEntry() {
		FolderEntry entry = getFolderEntry(".."); 
		Tool.assertOccur(entry == null , "folder " + getName() + "with no parent");
		return entry;
	}

	public FolderEntry getThisFolderEntry() {
		FolderEntry entry = getFolderEntry(".");
		Tool.assertOccur(entry == null, "folder " + getName() + "with no current entry");
		return entry;
	}


	
	public boolean hasEntry (String fileName) {
		return entryMap.containsKey(fileName);
	}

	/** use in both create and load, pay no attention to link */
	public void addEntry (String fileName, Inode inode) {
		//size ++; //add the entry with one
		FolderEntry entry = new FolderEntry(fileName, inode);
		entryMap.put(fileName, entry);
	}

	/** add an entry with specific filename and address to the folder */
	public void createEntry (String fileName, Inode inode)
	{
		inode.link(); //inode.linkCount++
		addEntry(fileName, inode);	
		//Tool.DBG("start to save in create entry");
		save();
	}

	/** remove an entry from the folder */
	public void removeEntry (String fileName)
	{
		Tool.assertOccur(!entryMap.containsKey(fileName), "Try to remove a not existed entry in folder " + getName());
		entryMap.get(fileName).inode.unlink(); //unlink the inode
		entryMap.remove(fileName);
		save();
		Tool.DBG("remove end");
	}


	public void print (String action) {
		System.out.println(action + " folder " + getName());
		System.out.println("size:" +size());
		Iterator<FolderEntry> iter = entryMap.values().iterator();
		while (iter.hasNext()) {
			FolderEntry entry = iter.next();
			System.out.println(entry.name + ":" + entry.inode.index);
		}
	}
	public void print () {
		print("");
	}
	/** save the content of the folder to the disk */
	public void save ()
	{
		print("save");
		seek(0);//move to the start of the file
		writeInt(size());
		writeString(folderName);
		Iterator<FolderEntry> iter = entryMap.values().iterator();
		while (iter.hasNext()) {
			//Tool.DBG("save entry");
			FolderEntry entry = iter.next();
			writeString(entry.name);
			writeInt(entry.inode.index);
		}
		//write entryMap to disk, and free the unneed entrys;
		//TODO implement this
	}

	/** load the content of the folder from the disk */
	public void load ()
	{
		seek(0);
		Tool.assertOccur(tell() != 0, "try to load a folder not from the init pos");
		entryMap.clear();
		//System.out.println(inode.fileSize);
		int size = readInt();
		//System.out.println("size" + size);
		folderName = readString(maxFileLength);
		//System.out.println(folderName + folderName.length());
		for (int i = 0; i < size; i++) {
			String name = readString(maxFileLength);
			//Tool.PR(name);
			int inodeIndex = readInt();
			Inode inode = fs.getInode(inodeIndex);
			addEntry(name, inode);
		}
		print("load");
		//TODO implement this
	}

	public static final int maxFileLength = 255;
}

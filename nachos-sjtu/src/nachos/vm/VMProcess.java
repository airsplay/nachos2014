package nachos.vm;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;
import nachos.userprog.UserKernel;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
	}
	TranslationEntry[] savedTLB;//get idea from Tian Boyu & Liu Yiming
	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		//PR("contextSwitch from<- " + this.pid);	
		savedTLB = VMKernel.pageManager().contextSwitch(this.pid);
		super.saveState();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		//System.out.println("contextSwitch into-> " + this.pid);	
		VMKernel.pageManager().restoreTLB(this.pid, savedTLB);
		//todo saved the tlb
		//super.restoreState();
	}
	public void PR(String s) {
		//System.out.println(s);	
	}

	public Byte readVirtualByte(int vAddr) {
		//todo directly lock readVirtualMemory
		pageLock.acquire();
		int pAddr = realize(vAddr);
		if (pAddr == -1) {
			pageLock.release();
			return null;
		}	
		VMKernel.pageManager().use(this.pid, Processor.pageFromAddress(vAddr));
		Byte resByte =  UserKernel.memoryManager.readByte(pAddr);
		pageLock.release();
		return resByte;
	}
	public boolean writeVirtualByte(int vAddr, byte data) {
		pageLock.acquire();
		int vpn = Processor.pageFromAddress(vAddr);
		System.out.println(this.pid + " " + vpn);
		if (isReadOnly(vpn)) {
			pageLock.release();
			return false;
		}
		int pAddr = realize(vAddr);	
		//if (pAddr == -1) {
		//	pageLock.release();
		//	return false;
		//}
		VMKernel.pageManager().use(this.pid, Processor.pageFromAddress(vAddr));
		VMKernel.pageManager().dirty(this.pid, Processor.pageFromAddress(vAddr));
		UserKernel.memoryManager.writeByte(pAddr, data); 
		pageLock.release();
		return true;
	}
	public void assertOccur(boolean b, String s) {
		if (b) {
			System.out.println("CRITICAL ERROR:"+s);
		}
	}
	protected boolean isReadOnly(int vpn) {
		TranslationEntry entry = VMKernel.pageManager().getTranslationEntry(this.pid, vpn);
		assertOccur(entry == null, "can't get the pid" + this.pid + " vpn" + vpn + "page for judge readonly");
		return entry.readOnly;
	}
	protected int realize(int vAddr) { //return the pAddr
		int vpn = Processor.pageFromAddress(vAddr);
		TranslationEntry entry = VMKernel.pageManager().getTranslationEntry(this.pid, vpn);
		assertOccur(entry == null, "can't get the pid" + this.pid + " vpn" + vpn + "page for realizing");
		return entry.ppn * pageSize + Processor.offsetFromAddress(vAddr);
	}
	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		VMKernel.pageManager().contextSwitch(getPpid());
		pageLock.acquire();
		int lastSectionVpn = -1;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				lastSectionVpn = vpn;
				System.out.println(pid + "section" + vpn + section.isReadOnly());
				VMKernel.pageManager().createSectionPage(this.pid, vpn, section, i);
//				pageTable[vpn].readOnly = section.isReadOnly();
//				section.loadPage(i, pageTable[vpn].ppn);
			}
		}
		for (int vpn = lastSectionVpn + 1; vpn<=lastSectionVpn + stackPages; vpn++) { //allocate stack pages
			System.out.println(pid + "stack" + vpn);
			VMKernel.pageManager().createStackPage(this.pid, vpn);
		}
		System.out.println(pid + "argument" + (numPages -1));
		VMKernel.pageManager().createArgumentPage(this.pid, numPages - 1);
		pageLock.release();
		return true;
		
	}

	protected boolean acquirePageTable() {
		numBytes = numPages * pageSize;
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		pageLock.acquire();
		VMKernel.pageManager().processFinish(this.pid, this.numPages);	
		pageLock.release();
	}

	//protected void cleanUp() {
	//	super.cleanUp();
	//}
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		PR(cause);
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			int vpn = Processor.pageFromAddress(Machine.processor().readRegister(Processor.regBadVAddr));
			assertOccur(vpn >= numPages, "an illigal vpn excced the limit" + vpn);
			//if (vpn > numPages) return;
			pageLock.acquire();
			//System.out.println("----^^^^^^^-------");
			VMKernel.pageManager().handleTLBMiss(this.pid, vpn);
			//System.out.println("----VVVVVVV-------");
			pageLock.release();
			break;
		default:
			super.handleException(cause);
			break;
		}
	}

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	//private static Lock loadLock = new Lock();
	private static Lock pageLock = new Lock();
}

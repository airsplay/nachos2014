package nachos.vm;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;
import nachos.userprog.UserKernel;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		//PR("contextSwitch from<- " + this.pid);	
		VMKernel.pageManager().contextSwitch(this.pid);
		super.saveState();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		//System.out.println("contextSwitch into-> " + this.pid);	
		//super.restoreState();
	}
	public void PR(String s) {
		System.out.println(s);	
	}

	public Byte readVirtualByte(int vAddr) {
		int pAddr = realize(vAddr);
		if (pAddr == -1) return null;
		VMKernel.pageManager().use(this.pid, Processor.pageFromAddress(vAddr));
		return UserKernel.memoryManager.readByte(pAddr);
	}
	public boolean writeVirtualByte(int vAddr, byte data) {
		int vpn = Processor.pageFromAddress(vAddr);
		System.out.println(this.pid + " " + vpn);
		if (isReadOnly(vpn)) {
			return false;
		}
		int pAddr = realize(vAddr);	
		if (pAddr == -1) return false;
		VMKernel.pageManager().use(this.pid, Processor.pageFromAddress(vAddr));
		VMKernel.pageManager().dirty(this.pid, Processor.pageFromAddress(vAddr));
		UserKernel.memoryManager.writeByte(pAddr, data); 
		return true;
	}
	protected boolean isReadOnly(int vpn) {
		TranslationEntry entry = VMKernel.pageManager().getTranslationEntry(this.pid, vpn);
		if (entry == null) {
			sendError("FUCK can't get the entry while judge readonly");
			return false;
		}
		return entry.readOnly;
	}
	protected int realize(int vAddr) {
		//PR("realizing");
		int vpn = Processor.pageFromAddress(vAddr);
		//PR(vpn);
		TranslationEntry entry = VMKernel.pageManager().getTranslationEntry(this.pid, vpn);
		if (entry == null) {
			sendError("FUCK can't get the entry while realize the vpn");
			return -1;
		}
		else return entry.ppn * pageSize + Processor.offsetFromAddress(vAddr);
	}
	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		loadLock.acquire();
		int lastSectionVpn = -1;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				lastSectionVpn = vpn;
				System.out.println(pid + "section" + vpn + section.isReadOnly());
				VMKernel.pageManager().createSectionPage(this.pid, vpn, section, i);
//				pageTable[vpn].readOnly = section.isReadOnly();
//				section.loadPage(i, pageTable[vpn].ppn);
			}
		}
		for (int vpn = lastSectionVpn + 1; vpn<=lastSectionVpn + stackPages; vpn++) { //allocate stack pages
			System.out.println(pid + "stack" + vpn);
			VMKernel.pageManager().createStackPage(this.pid, vpn);
		}
		System.out.println(pid + "argument" + (numPages -1));
		VMKernel.pageManager().createArgumentPage(this.pid, numPages - 1);
		loadLock.release();
		return true;
		
	}

	protected boolean acquirePageTable() {
		numBytes = numPages * pageSize;
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		VMKernel.pageManager().processFinish(this.pid, this.numPages);	
	}

	//protected void cleanUp() {
	//	super.cleanUp();
	//}
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			int vpn = Processor.pageFromAddress(Machine.processor().readRegister(Processor.regBadVAddr));
			//if (vpn > numPages) return;
			VMKernel.pageManager().handleTLBMiss(this.pid, vpn);
			break;
		default:
			super.handleException(cause);
			break;
		}
	}

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	private static Lock loadLock = new Lock();
}

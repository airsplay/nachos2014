package nachos.filesys;
public class SymLink extends File {

	String path;
	public SymLink(Inode inode, RealFileSystem fs) {
		super(inode, "", fs);
	}
	
	public SymLink(Inode inode, String name, RealFileSystem fs) {
		super(inode, name, fs);
	}

	public void init(String path) {
		this.path = path;
		save();
	}

	public Inode getTargetInode() {
		/** get conjugate */
		RealFileSystem.Conjugate conj = fs.getConjugate(null, path);//always an absolute path
		if (conj == null) {
			return null;
		}
		/** check the existance of the name */
		if (!conj.folder.hasEntry(conj.name)) {
			return null;
		}
		FolderEntry entry = conj.folder.getFolderEntry(conj.name);
		return entry.inode;
	}

	public void save () {
		seek(0);
		writeString(path);
	}

	public void load () {
		Tool.assertOccur(tell() != 0, "try to load a symlink from a non-zero place");
		path = readString(maxFileLength);
	}
}

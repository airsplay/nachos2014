package nachos.filesys;
import nachos.machine.Lib;

public class IOIterator {
	int sector;
	int pos;
	RealFileSystem fs;
	byte[] buffer = new byte[4];
	public IOIterator (int sector, int initPos, RealFileSystem fs) {
		this.sector = sector;
		this.pos = initPos;
		this.fs = fs;
	}

	public void writeInt(int  value) {
		Lib.bytesFromInt(buffer, 0, value);
		fs.writeSector(sector, pos, pos + 4, buffer, 0);
		pos += 4;
	}
	public int readInt() {
		fs.readSector(sector, pos, pos + 4, buffer, 0);
		pos += 4;
		return Lib.bytesToInt(buffer, 0);
	}
}

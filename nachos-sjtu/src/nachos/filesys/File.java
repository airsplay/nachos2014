package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.OpenFile;
import nachos.machine.Lib;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
	public Inode inode;

	private int pos;
	RealFileSystem fs;
	public File (Inode inode, String name, RealFileSystem fs) {
		super(fs, name);
		this.fs = fs;
		this.inode = inode;

	}

	public Inode getInode() {
		return inode;
	}

	@Override
	public RealFileSystem getFileSystem() {
		return fs;
	}
	

	public File (Inode inode, RealFileSystem fs)
	{
		super(fs, "ananymous");
		this.fs = fs;
		this.inode = inode;
		pos = 0;
	}

	public void use() {
		inode.use();
	}

	public void unuse() {
		inode.unuse();
	}
	public void assertOccur(boolean b, String s){
		if (b) {
			System.out.println(s);
		}
	}

	public int length ()
	{
		return inode.fileSize();
	}

	public void close ()
	{
		inode.unuse();
	}

	public void seek (int pos)
	{
		Tool.assertOccur(pos > length() || pos < 0, "want to seek an unReachable pos in file " + getName());
		this.pos = pos;
	}

	public int tell ()
	{
		return pos;
	}
	
	public String readString(int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		int init = tell ();		
		byte[] bytes = new byte[maxLength + 1];
		int bytesRead = read(bytes, maxLength + 1);
		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0) {
				seek(init + length + 1);//stringLength + "0"
				return new String(bytes, 0, length);
			}
		}
		Tool.assertOccur(true, "an unsuccessful read string");
		seek(init);
		return null;
	}
	public Integer readInt() {
		byte[] bytes = new byte[4];
		int init = tell ();		
		int length = read(bytes, 4);
		if (length == -1) {
			seek(init);
			Tool.assertOccur(true, "an unsuccessful read int");
			return null;
		}
		else if (length < 4) {
			Tool.assertOccur(true, "an unsuccessful read int");
			seek(init);
			return null;
		}
		else {
			seek(init + 4);
			return new Integer(Lib.bytesToInt(bytes, 0));
		}
	}
	public void PR(String s){
		System.out.println(s);
	}
	public void writeString(String string) {
		byte[] buffer = (string+"\0").getBytes();
		//PR("writeString" + buffer.length);
		write(buffer, buffer.length);	
	}

	public void writeInt(int i) {
		byte[] buffer = new byte[4];
		Lib.bytesFromInt(buffer, 0, i);
		write(buffer, 4);
	}

	public int read (byte[] buffer, int limit) {
		return read(buffer, 0, limit);	
	}

	public int write(byte[] buffer, int limit) {
		return write(buffer, 0, limit);
	}

	public int read (byte[] buffer, int start, int limit)
	{
		int ret = read(pos, buffer, start, limit);
		pos += ret;
		return ret;
	}

	public int write (byte[] buffer, int start, int limit)
	{
		int ret = write(pos, buffer, start, limit);
		pos += ret; //base on write bytes are continous
		return ret;
	}

	public int read (int pos, byte[] buffer, int start, int limit)
	{
		return wrKernel(pos, buffer, start, limit, readFlag);
	}

	public int write (int pos, byte[] buffer, int start, int limit)
	{
		return wrKernel(pos, buffer, start, limit, writeFlag);
	}

	/**the kernel to operate read & write
	 * @param wrFlag
	 *			0:read, 1:write
	 **/
	public int wrKernel(int pos, byte[] buffer, int start, int limit, int wrFlag) {
		int posNow = pos;
		int bufferNow = start;
		int bufferEnd;
		if (wrFlag == readFlag) //a preservable reading
			bufferEnd = start + Math.min(limit, length() - pos); 
		else {
			bufferEnd = start + limit;
			inode.setFileSize(pos + limit);
		}
		int success = 0;
		//reading from pos to buffer [start, bufferEnd)
		while (bufferNow < bufferEnd) {
			int sector = inode.getSectorFromPos(posNow);
			if (sector == -1) {
				break;
			}
			int localBegin = inode.getOffsetFromPos(posNow);
			int localEnd = Math.min(sectorSize, localBegin + bufferEnd - bufferNow);
			int localSuccess;
			/**buffer[bufferNow, ] = Sector[localBegin, localEnd) */
			if (wrFlag == writeFlag) 
				localSuccess = getFileSystem().writeSector(sector, localBegin, localEnd, buffer, bufferNow);//use this kind of readSector:localEnd will not exceed sectorSize
			else 
				localSuccess = getFileSystem().readSector(sector, localBegin, localEnd, buffer, bufferNow);
			success += localSuccess;
			int jump = localEnd - localBegin;
			bufferNow = bufferNow + jump;
			posNow = posNow + jump;
		}
		return success;
	}

	public final static int sectorSize = Disk.SectorSize;
	public final static int writeFlag = 1; //a similar approach compare with fd
	public final static int readFlag = 0;

	public static final int maxFileLength = 255;
}

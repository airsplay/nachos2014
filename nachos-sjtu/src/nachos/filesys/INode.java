package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class Inode
{
	
	/** represent a folder */
	public final static int TYPE_FOLDER = 1;
	/** represent a normal file */
	public final static int TYPE_FILE = 2;
	/** represent a normal file that is marked as delete */
	public final static int TYPE_FILE_DEL = 3;
	/** represent a symbolic link file */
	public final static int TYPE_SYMLINK = 4;
	/** represent a folder that are not valid */
	public final static int TYPE_FOLDER_DEL = 5;

	public final static int TYPE_EXTEND = 6;

	public final static int savedSize = 4 * 4;
/**config. can't be changed whill running*/
	int numSectors; //the number of sector in one inode
	int savedSector;
	int savedPos;
	int index;
/**runtime configuration.*/ 
	/**saved*/
		/**critical */
	int fileSize;
	int linkCount;
	Inode nextInode;	
	int[] sectors;
		/**not critical. don't need in clear*/
	int fileType;
	/**not saved */
	int useCount;
	RealFileSystem fs;

	/** init an inode for use, already link it. 
	 * At the same time, you must explicit call use to add use. 
	 * No method will help you to set use, and this is only method do with the linkCount
	 */
	public void init(int fileType){ //call to init this inode to particular fileType
		
		fileSize = 0;
		linkCount = 0;
		nextInode = null;
		for (int i = 0; i < numSectors; i++ ) sectors[i] = -1;
		this.fileType = fileType;
		save();

		useCount = 0;
	}

	public void clear() { //clean this inode. I think this is meaningless, and should not be called anywhere;
		fileSize = linkCount = useCount = 0;
		nextInode = null;
		for (int i = 0; i < numSectors; i++ ) sectors[i] = -1;
		save();
	}

	/**create a new Inode. It has no reponsibility to set the data. If you want to LOAD it from Disk, use load, and if you want to use it as a new inode, use init() 
	 * @param index the index of this inode. It's used to save this inode to disk.
	 * @param savedSector
	 * @param savedPos save this inode to position (savedSector, savedPos) in disk.
	 * @param size the meta size of this inode. Judge by format.
	 * @param fs in which file system
	 */
	public Inode(int index, int savedSector, int savedPos, int size, RealFileSystem fs) {
		this.savedSector = savedSector;
		this.savedPos = savedPos;
		this.index = index;
		this.fs = fs;
		numSectors = (size - savedSize) / 4; //a magic number for one int
		sectors = new int[numSectors];
		//clear();
	}
	
	
	/** get the sector number of a position in the file. if the pos doesn't exist, then create a new one, as well as the next inode
	 * @param pos the pos in this virtual file
	 * @return the sector of this pos. -1 if no such sector
	 */
	public int getSectorFromPos (int pos)
	{
		//boolean createOne = false;//for assert
		/** go to the correct inode*/
		Inode nowInode = this;
		while (pos >= numSectors * Disk.SectorSize) {//if pos == (numBytes in this inode), I must go to the next Inode
			//Tool.DBG("occupy new inode");
			if (nowInode.nextInode == null) {
				for (int i = 0; i < nowInode.numSectors; i++) {//each time create a new nextInode, flush the old inode's sectors
					if (nowInode.sectors[i] == -1) {
						nowInode.sectors[i] = fs.getFreeSector();
						//Tool.assertOccur(true, "Create a cheat sector in inode " + nowInode.index + " sector " + i);
					}
				}
				nowInode.nextInode = fs.getFreeInode();
				nowInode.nextInode.init(TYPE_EXTEND);
				//createOne = true;//for assert
			}
			pos -= nowInode.numSectors * Disk.SectorSize;
			nowInode = nowInode.nextInode;
		}
		//Tool.DBG("POS" + pos);
		/** get the sector, if doesn't exist, create one */
		for (int i = 0; i <= pos/Disk.SectorSize; i++) {
			//Tool.DBG("get free sector" + i);
			if (nowInode.sectors[i] == -1) {
				nowInode.sectors[i] = fs.getFreeSector();
			}
		}
		/*if (nowInode.sectors[pos / Disk.SectorSize] == -1) {
			Tool.assertOccur(createOne && (pos/Disk.SectorSize != 0), "uncountinuous getPos, I handled it, but must check it");
		}*/

		return nowInode.sectors[pos / Disk.SectorSize];
	}

	public int getOffsetFromPos (int pos) {
		return pos % Disk.SectorSize;
	}
	/** change the file size and adjust the content in the inode accordingly */
	public void setFileSize (int newSize)
	{
		/** the fileSize already bigger then newSize, don't need to append */
		if (fileSize > newSize) {
			return;
		}
		//getSectorFromPos(newSize - 1);
		fileSize = newSize;
		save();
		/*//find nowInode	
		Inode nowInode = this;
		while (fileSize > numSectors * Disk.SectorSize) {
			Tool.assertOccur(nextInode == 0, "the fileSize doesn't match in inode");
			nowInode = nowInode.nextInode;
		}*/
		/*	
		tmpPos = fileSize;//create [fileSize, size - 1].  I call {fileSize, fileSize+sectorSize, fileSize+sectorSize*2..., newSize-1}
		while (tmpPos < newSize) {
			getSectorFromPos(tmpPos);
			tmpPos += Disk.SectorSize;
		}
		getSectorFromPos(newSize - 1);
		fileSize = newSize;
		*/
	}

	public int fileSize() {
		return fileSize;
	}

	public void use() {
		useCount ++;
		save();
	}

	public void unuse() {
		useCount --;
		System.out.println("unuse inode:" + index + "useCount:" + useCount + "linkCount:" + linkCount);
		if (useCount == 0 && (/*fileType == TYPE_FOLDER_DEL ||*/ fileType == TYPE_FILE_DEL)) {
			free();
		}
	}

	public void link() {
		linkCount ++;
		save();
	}

	/** unlink this inode */
	public void unlink() {
		linkCount --;
		System.out.println("unlink inode:" + index + "linkCount:" + linkCount+ "useCount:" + useCount);
		switch (fileType) {
			case TYPE_FILE:
				if (linkCount == 0) {
					fileType = TYPE_FILE_DEL;
					save();
					if (useCount == 0) {
						free();
					}
				}
				return;
			case TYPE_SYMLINK:
				free();
				return;
			case TYPE_FOLDER:
				if (linkCount == 1) {
					fileType = TYPE_FOLDER_DEL;
					free();
					//save();
					//if (useCount == 0) {
					//	free();
					//}
				}
				return;
			case TYPE_FOLDER_DEL:
				Tool.assertOccur(linkCount != 0, "try to unlink an already unlink folder");
				return;
			case TYPE_FILE_DEL:
				Tool.assertOccur(true, "try to unlink an already unlinked file");
				return;
			default:
					Tool.assertOccur(true, "try to unlink an inode with illegel type");
		}
		save();
	}

	/** free the disk space occupied by the file (including inode) */
	public void free ()
	{
		/**todo:free the '.' and '..' */
		if (fileType == TYPE_FOLDER_DEL) {
			Folder folder = new Folder(this, fs);
			folder.load();
			folder.clear();
		}
		///** free the meta data in inode's content */
		//Folder folder = new Folder(this, fs);
		//folder.load();
		//folder.clear();
		/** free inode recursively */
		if (nextInode != null) {
			nextInode.free();
		}
		/** free this inode by type */
		for (int i = 0; i < numSectors; i ++ ) {
			if (sectors[i] != -1) {
				fs.freeSector(sectors[i]);
			}
		}
		fs.freeInode(this);//because an inode must be inited or loaded before used, so I am free to clear it
	}
	
	public int getIndex() {
		return index;
	}

	/** save inode content from the disk */
	public void save () {
		IOIterator iter = fs.getIOIterator(savedSector, savedPos);
		iter.writeInt(fileSize);
		iter.writeInt(linkCount);
		if (nextInode == null)
			iter.writeInt(-1);
		else 
			iter.writeInt(nextInode.index);
		for (int i = 0; i < sectors.length; i++) {
			iter.writeInt(sectors[i]);
		}
		iter.writeInt(fileType);
	}

	/** load inode content to the disk */
	public void load () {
		IOIterator iter = fs.getIOIterator(savedSector, savedPos);
		fileSize = iter.readInt();
		linkCount = iter.readInt();
		int nextInodeIndex = iter.readInt();
		if (nextInodeIndex == -1)
			nextInode = null;
		else
			nextInode = fs.getInode(nextInodeIndex);
		for (int i = 0; i < sectors.length; i++) {
			sectors[i] = iter.readInt();
		}
		fileType = iter.readInt();
	}
	public void print () {
		System.out.println("index :" + index);
		if (nextInode != null)
			System.out.println("nextInodeinodex :" + nextInode.index);
		for (int i = 0; i < sectors.length; i++) {
			if (sectors[i] != -1) 
				System.out.println("sector " + i + ":" + sectors[i]);
		}
		System.out.println("filetype" + fileType);
	}
}

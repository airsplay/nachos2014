package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.FileSystem;
import nachos.machine.Machine;
import nachos.machine.OpenFile;


/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
	/** the root folder */
	private Folder rootFolder;

	/** the string representation of the current folder */
	private LinkedList<String> cur_path = new LinkedList<String>();

	/** just renaming */
	public final static int sectorSize = Disk.SectorSize;

	public final static int formatSector = 0;
	
	private byte[] fsBuffer; 
	
	private FreeManager sectorFreeManager;
	private FreeManager inodeFreeManager;
	
	//private Lock fsLock = new Lock();

	private byte[] disk;
	public class Format {
		private final static int formatSector = 0;
		private final static int kbytePerInode = 2;//every kbytePerInode allocate an inode
		//public int numSectors;
		public int numInodes;
		public int inodeSize; //byte per inode

		public int freeInodeListSector;
		public int freeSectorListSector;
		public int inodeStartSector;

		public int rootInodeIndex;
		//public int rootSector;
		public void init() {
			numInodes = Disk.NumSectors * Disk.SectorSize / 1024 / kbytePerInode;
			inodeSize = Disk.SectorSize / 4;
			freeInodeListSector = 1;
			freeSectorListSector = 2;
			inodeStartSector = 3;
			save();
		}
		
		public void print() {
			System.out.println("numInodes:" + numInodes);
			System.out.println("inodeSize:" + inodeSize); //byte per inode

			System.out.println("freeInodeListSector:" + freeInodeListSector);
			System.out.println("freeSectorListSector:" + freeSectorListSector);
			System.out.println("inodeStartSector:" + inodeStartSector);

			System.out.println("rootInodeIndex:" + rootInodeIndex);
		}

		public void setRootInodeIndex(int rootInodeIndex) {
			this.rootInodeIndex = rootInodeIndex;
			save();
		}

		public void save() {
			IOIterator iter = getIOIterator(formatSector, 0);
			iter.writeInt(numInodes);
			iter.writeInt(inodeSize); //byte per inode

			iter.writeInt(freeInodeListSector);
			iter.writeInt(freeSectorListSector);
			iter.writeInt(inodeStartSector);

			iter.writeInt(rootInodeIndex);
		}

		public void load() {
			IOIterator iter = getIOIterator(formatSector, 0);
			numInodes = iter.readInt();
			inodeSize = iter.readInt(); //byte per inode

			freeInodeListSector = iter.readInt();
			freeSectorListSector = iter.readInt();
			inodeStartSector = iter.readInt();

			rootInodeIndex = iter.readInt();
		}
	}

	public Format format;

	private Inode[] inodes;


	public RealFileSystem () {
		disk = new byte[Disk.NumSectors * Disk.SectorSize];
		fsBuffer = new byte[sectorSize];
	}

	/** set all zero*/	
	public void eraseSector(int sector) {
		Tool.DBG("erase sector " + sector);
		writeSector(sector, 0, sectorSize, fsBuffer, 0);
	}
	public void eraseDisk() {
		for (int i = 0; i < sectorSize; i++) {
			fsBuffer[i] = 0;
		}
		for (int i = 0; i < 300/*Disk.NumSectors*/; i++) {
			eraseSector(i);
		}
	}

	/**use the format configuration to format this disk*/
	public void formatDisk() {
		Tool.PR("format disk");
		/**clear the disk to 0*/
		//eraseDisk();
		/**create the format*/
		format = new Format();
		format.init();
		/**Occupy meta sectors and meta inodes:freeSector & freeInode & inodes */
		Tool.PR("load Free Managers");
		sectorFreeManager = new FreeManager(format.freeSectorListSector, Disk.NumSectors, this);
		sectorFreeManager.init();
		sectorFreeManager.occupy(Format.formatSector);
		sectorFreeManager.occupy(format.freeSectorListSector);
		sectorFreeManager.occupy(format.freeInodeListSector);
		int nowPos = format.inodeStartSector * sectorSize;
		for (int i = 0; i < format.numInodes; i++) {
			sectorFreeManager.occupy(nowPos/sectorSize); //multi occupy is permmitted
			nowPos += format.inodeSize;
		}
		sectorFreeManager.save();

		inodeFreeManager = new FreeManager(format.freeInodeListSector, format.numInodes, this);
		inodeFreeManager.init();
		/** create the root Folder */
		Tool.PR("create root folder");
		int rootInodeIndex = inodeFreeManager.allocate();
		format.setRootInodeIndex(rootInodeIndex);
		Tool.PR("set compelete");
		int rootInodePos = format.inodeStartSector * sectorSize + rootInodeIndex * format.inodeSize;
		Inode rootInode = new Inode(rootInodeIndex, rootInodePos / sectorSize, rootInodePos % sectorSize, format.inodeSize, this);
		Tool.PR("rootInode init start");
		rootInode.init(Inode.TYPE_FOLDER);
		Tool.PR("rootInode init complete");
		Folder rootFolder = new Folder(rootInode, "root", this);
		Tool.PR("rootFolder init start");
		rootFolder.init();
		Tool.DBG("sdjifos");
		rootFolder.setParent(rootFolder);
		Tool.PR("format disk complete");
		inodeFreeManager.print();
		inodeFreeManager.save();
		inodeFreeManager.load();
		inodeFreeManager.print();
	}

	/**
	 * initialize the file system
	 * 
	 * @param format
	 *          whether to format the file system
	 */
	public void init (boolean needFormat)
	{
		if (needFormat) {
			formatDisk();
		}
		Tool.PR("start loading");
		/**load format*/
		format.load();
		format.print();
		/**load two free Manager (sectos and inodes)*/
		Tool.PR("load free managers");
		inodeFreeManager = new FreeManager(format.freeInodeListSector, format.numInodes, this);
		sectorFreeManager = new FreeManager(format.freeSectorListSector, Disk.NumSectors, this);
		inodeFreeManager.load();
		sectorFreeManager.load();
		inodeFreeManager.print();
		/**initial the inode stub */
		Tool.PR("create inode stub");
		inodes = new Inode[format.numInodes];
		int nowPos = format.inodeStartSector * sectorSize;
		for (int i = 0; i < format.numInodes; i++) {
			inodes[i] = new Inode(i, nowPos / sectorSize, nowPos % sectorSize, format.inodeSize, this);
			nowPos += format.inodeSize;
		}
		Tool.PR("load inodes");
		/**load non-free inode from Disk*/
		for (int i = 0; i < format.numInodes; i++) {
			if (inodeFreeManager.isFree(i)) {

			}
			else {
				Tool.PR("occupied inode"+i);
				inodes[i].load();
			}
		}
		Tool.PR("load root Folder");
		/**load the root folder*/
		getInode(format.rootInodeIndex).print();
		rootFolder = new Folder(getInode(format.rootInodeIndex), this);
		rootFolder.load();
		Tool.PR("end loading ,start import");
		/** import the stub fileSystem */	
		importStub();
		Tool.PR("end importing");
	}

	public Folder getRootFolder() {
		return rootFolder;
	}

	public void finish ()
	{
	}

	/** get the inode instance
	 * @param index
	 * @return the instance of the index inode
	 */
	public Inode getInode(int index) {
		return inodes[index];
	}

	public Inode getFreeInode() {
		int inodeIndex = inodeFreeManager.allocate();
		return getInode(inodeIndex);
	}

	public int getFreeSector() {
		return sectorFreeManager.allocate();
	}

	public void freeSector(int index) {
		sectorFreeManager.free(index);
	}

	public void freeInode(Inode inode) {
		inodeFreeManager.free(inode.getIndex());
	}

	/** import from stub filesystem */
	private void importStub ()
	{
		FileSystem stubFS = Machine.stubFileSystem();
		RealFileSystem realFS = FilesysKernel.realFileSystem;
		String[] file_list = Machine.stubFileList();
		for (int i = 0; i < file_list.length; ++i)
		{
			if (!file_list[i].endsWith(".coff"))
				continue;
			OpenFile src = stubFS.open(file_list[i], false);
			if (src == null)
			{
				continue;
			}
			OpenFile dst = realFS.open(rootFolder, file_list[i], true);
			int size = src.length();
			byte[] buffer = new byte[size];
			src.read(0, buffer, 0, size);
			dst.write(0, buffer, 0, size);
			src.close();
			dst.close();
		}
	}


	public IOIterator getIOIterator(int sector, int initPos) {
		return new IOIterator(sector, initPos, this);
	}
	/** read sector from the disk 
	 * @param sector: the id of the sector
	 * @param localBegin: start of sector
	 * @param localEnd: end of sector
	 * @param buffer: 
	 * @param offset start pos of buffer
	 * @return the size of success read
	 */
	public int readSector(int sector, int localBegin, int localEnd, byte[] buffer, int offset) {
		//System.out.println("read " + sector);
		Tool.assertOccur(localEnd > Disk.SectorSize || localEnd < 0, "wrong addresss while writing sector");
		Tool.assertOccur(offset + localEnd - localBegin > buffer.length, "exceed buffer in readSector");
//		Machine.synchDisk().readSector(sector, fsBuffer, 0);
//		arrayCopy(buffer, offset, fsBuffer, localBegin, localEnd - localBegin);
		boolean status = Machine.interrupt().disable();
		Machine.interrupt().restore(status);	
		for (int i = localBegin; i < localEnd; i++) {
			buffer[offset + i - localBegin] = disk[sector * Disk.SectorSize + i];
		}
		return (localEnd - localBegin);
	}

	/** read sector from the disk 
	 * @param sector: the id of the sector
	 * @param localBegin: start of sector
	 * @param localEnd: end of sector
	 * @param buffer: 
	 * @param offset start pos of buffer
	 * @return the size of success read
	 */
	public int writeSector(int sector, int localBegin, int localEnd, byte[] buffer, int offset) {
		//System.out.println("write " + sector);
		Tool.assertOccur(localEnd > Disk.SectorSize || localEnd < 0, "wrong addresss while writing sector");
		Tool.assertOccur(offset + localEnd - localBegin > buffer.length, "exceed buffer in writeSector");
//		Machine.synchDisk().readSector(sector, fsBuffer, 0);
//		arrayCopy(fsBuffer, localBegin, buffer, offset, localEnd - localBegin);
//		Machine.synchDisk().writeSector(sector, fsBuffer, 0);
		boolean status = Machine.interrupt().disable();
		Machine.interrupt().restore(status);	
		for (int i = localBegin; i < localEnd; i++) {
			 disk[sector * Disk.SectorSize + i] = buffer[offset + i - localBegin];
		}
		return (localEnd - localBegin);
	}

	public void arrayCopy(byte[] a, int startA, byte[] b, int startB, int length) {
		for (int i = 0; i < length; i++) {
			a[startA + i] = b[startB + i];
		}
	}


	public OpenFile open (String name, boolean create) {
		Tool.assertOccur(true, "fucking machine class. call realfilesystem open");
		return null;
	}
	public boolean remove (String name) {
		Tool.assertOccur(true, "fucking machine class. call realfilesystem remove");
		return false;
	}
	public File open (Folder currentFolder, String fileNameWithPath, boolean create)
	{
		/** get the correct path and name */
		Conjugate conj = getConjugate(currentFolder, fileNameWithPath);
		//if (conj == null) Tool.DBG("null conj");	
		if (conj == null) return null;
		/** check if it doesn't has this entry */ 
		if (!conj.folder.hasEntry(conj.name)) {
			if (create) {  
				Inode inode = getFreeInode(); //create a new inode to contain this file
				inode.init(Inode.TYPE_FILE);
				conj.folder.createEntry(conj.name, inode);
			}
			else {
				return null;
			}
		}
		/** get the real inode */
		FolderEntry entry = conj.folder.getFolderEntry(conj.name);
		Inode realInode = getRealInode(entry.inode);
		if (realInode == null) {
			return null;
		}
		/** open the file */
		File returnFile;
		int type = realInode.fileType;
		if (type == Inode.TYPE_FOLDER) {
			return null;
		}
		else if (type == Inode.TYPE_FILE) {
			returnFile = new File(realInode, entry.name, this);
		}
		else if (type == Inode.TYPE_SYMLINK) {
			Tool.assertOccur(true, "wrong getRealInode Approach");
			return null; //an unsafe operation
		}
		else {
			return null;
		}
		/** use this file and return it */
		returnFile.use();
		return returnFile;
	}

	public boolean remove (Folder currentFolder, String fileNameWithPath)
	{
		/** get the correct path and name */
		Conjugate conj = getConjugate(currentFolder, fileNameWithPath);
		if (conj == null) return false;
		/** If no such name in currentFolder*/
		if (!conj.folder.hasEntry(conj.name)) {
			return false;
		}
		/** remove it. if it is a symlink, remove the symlink inode, not the realInode */
		FolderEntry entry = conj.folder.getFolderEntry(conj.name);
		int type = entry.inode.fileType;
		if (type == Inode.TYPE_FOLDER) {
			return false;
		}
		else if (type == Inode.TYPE_FILE) {
			conj.folder.removeEntry(conj.name);
		}
		else if (type == Inode.TYPE_SYMLINK) {
			conj.folder.removeEntry(conj.name);
		}
		else {
			return false;
		}
		return true;
	}

	public Inode getRealInode(Inode inode) {
		if (inode == null) {
			return null;
		}
		if (inode.fileType == Inode.TYPE_SYMLINK) {
			SymLink symLink = new SymLink(inode, this);
			symLink.load();
			return getRealInode(symLink.getTargetInode());
		}
		else return inode;
	}

	public class Conjugate {
		public Folder folder;
		public String name;
		public Conjugate(Folder folder, String name) {
			this.folder = folder;
			this.name = name;
		}
	}
	
	/** Go to the next Folder without increment the use of the inode.
	 * @param currentFolder the current folder it is in
	 * @param nextFolderName the name of folder is to be opened
	 * @return the next Folder, if no such folder or wrong, return null
	 */
	public Folder gotoFolder(Folder currentFolder, String nextFolderName) {
		/** check if has this entry */
		if (!currentFolder.hasEntry(nextFolderName)) {
			return null;
		}
		/** get the real inode of this entry */
		FolderEntry entry = currentFolder.getFolderEntry(nextFolderName);
		Inode inode = entry.inode;
		inode = getRealInode(inode);
		/** check if the inode is actually a folder */
		if (inode.fileType != Inode.TYPE_FOLDER) {
			return null;
		}
		Folder returnFolder =  new Folder(entry.inode, entry.name, this);
		returnFolder.load();
		return returnFolder;
	}
	
	public Conjugate getConjugate(Folder currentFolder, String nameWithPath) {
		/** icheck if end with a '/' */
		if (nameWithPath.charAt(nameWithPath.length() - 1) == '/') {
			return null;
		}
		/** check if it is start from root */
		if (nameWithPath.charAt(0) == '/') {
			currentFolder = getRootFolder();
			nameWithPath = nameWithPath.substring(1);//eliminate the first splash
		}
		else if (currentFolder == null) {
			return null;
		}
		currentFolder.load();
		/** split the path with '/' and parse it */
		String [] pathAndName = nameWithPath.split("/");
		for (int i = 0; i < pathAndName.length; i++) {
			Tool.DBG(i + ":" + pathAndName[i]);
		}
		for (int i = 0; i < pathAndName.length - 1; i ++) { //exclude the last String
			currentFolder = gotoFolder(currentFolder, pathAndName[i]);
			if (currentFolder == null) {
				return null;
			}
		}
		/** check if the folder has been already removed. Must follow the operation set root.
		 * you can't do anything under a folder which has been deleted
		 */
		if (currentFolder.inode.fileType != Inode.TYPE_FOLDER) {
			return null; 
		}
		return new Conjugate(currentFolder, pathAndName[pathAndName.length - 1]);
	}

	/** create a folder with folderName under parentFolder 
	 * @param parentFolder 
	 * @param folderName
	 * @return 0:success -1:unsuccess
	 */
	public int createFolder (Folder currentFolder, String folderNameWithPath) {
		/** get the correct path and name */
		Conjugate conj = getConjugate(currentFolder, folderNameWithPath);
		if (conj == null) return -1;
		/** check if the conj.name is used */
		if (conj.folder.hasEntry(conj.name)) {
			return -1;
		}
		/** create inode and set inode. */
		Inode inode = getFreeInode();
		inode.init(Inode.TYPE_FOLDER);
		conj.folder.createEntry(conj.name, inode);
		/** initialize the new folder */
		Folder newFolder = new Folder(inode, conj.name, this);
		newFolder.init();
		newFolder.setParent(conj.folder);
		return 0;
	}

	/** remove a folder 
	 * @param currentFolder
	 * @param folderNameWithPath the folder name may have several splash to indicate the path 
	 * @return 0:success -1:unsuccess
	 */
	public int removeFolder (Folder currentFolder, String folderNameWithPath) {
		/** get the correct path and name */
		Conjugate conj = getConjugate(currentFolder, folderNameWithPath);
		if (conj == null) return -1;	
		/** check if no such entry in currentFolder */
		if (!conj.folder.hasEntry(conj.name)) {
			return -1;
		}
		/** can't remove ".." and "." */
		if (conj.name == ".." || conj.name == ".") return -1;
		/** check if the inode is a folder*/
		FolderEntry entry = conj.folder.getFolderEntry(conj.name);
		Inode inode = entry.inode; 
		Tool.assertOccur(inode.fileType == Inode.TYPE_SYMLINK, "shabi test case want to remove a symlink to a folder?");
		if (inode.fileType != Inode.TYPE_FOLDER) {
			return -1;
		}
		/** check if the folder is empty */
		Folder removeFolder = new Folder(inode, this);
		removeFolder.load();
		if (!removeFolder.isEmpty()) {
			return -1;
		}
		/** unlink the folder */
		conj.folder.removeEntry(conj.name);
		//inode.unlink();
		return 0;	
	}

	/** change the current folder
	 * @param currentFolder 
	 * @param folderNameWithPath 
	 * @return the next folder on success, null on unsuccess
	 */
	public Folder changeCurrentFolder (Folder currentFolder, String folderNameWithPath) {
		/** get the correct path and name */
		Conjugate conj = getConjugate(currentFolder, folderNameWithPath);
		if (conj == null) return null;
		/** go to the next folder*/
		Folder newFolder = gotoFolder(conj.folder, conj.name);
		if (newFolder == null){
			return null;
		}
		/** successfully change the folder, set use and unuse */
		if (currentFolder != null) currentFolder.unuse();
		newFolder.use();
		return newFolder;
	}


	public String[] readDir (String name)
	{
		return null;
	}

	public FileStat getStat (String name)
	{
		return null;
	}

	/** make a hard link dst represent src
	 * @param currentFolder
	 * @param src 
	 * @param dst
	 * @return 0 on success, -1 o.w.
	 */
	public int createLink (Folder currentFolder, String src, String dst)
	{
		/** get the correct path and name about src and dst */
		Conjugate srcConj = getConjugate(currentFolder, src);
		Conjugate dstConj = getConjugate(currentFolder, dst);
		if (srcConj == null || dstConj == null) {
			return  -1;
		}
		/** check src file exist */
		if (!srcConj.folder.hasEntry(srcConj.name)) {
			return -1;
		}
		FolderEntry srcEntry = srcConj.folder.getFolderEntry(srcConj.name);
		/** get src's real inode (across symlink) */
		Inode srcInode = getRealInode(srcEntry.inode);
		/** check if already exist a dst */
		if (dstConj.folder.hasEntry(dstConj.name)) {
			return -1;
		}
		/** link dst to src */
		dstConj.folder.createEntry(dstConj.name, srcInode); //add entry has done with linkCount
		//dstConj.folder.print("link");
		return 0;
	}

	/** make a soft link dst to src. Only hard link need symlink unrool. symlink just link to the correct path
	 * @param currentFolder
	 * @param src 
	 * @param dst 
	 * @return 0 on success, -1 o.w.
	 */
	public int createSymLink (Folder currentFolder, String src, String dst)
	{
		/** get the correct path and name about src and dst */
		Conjugate srcConj = getConjugate(currentFolder, src);
		Conjugate dstConj = getConjugate(currentFolder, dst);
		if (srcConj == null || dstConj == null) {
			return  -1;
		}
		/** get src path string */
		String srcPathString = getPathStringRec(srcConj.folder) + srcConj.name;
		/** don't need to check the existance or fetch the real inode of src. a path is enough */
		///** check src file exist */
		//if (!srcConj.folder.hasEntry(srcConj.name)) {
		//	return -1;
		//}
		//FolderEntry srcEntry = srcConj.folder.getFolderEntry(srcConj.name);
		///** get src's real inode (across symlink) */
		//Inode srcInode = getRealInode(srcEntry.inode);

		/** check if already exist a dst */
		if (dstConj.folder.hasEntry(dstConj.name)) {
			return -1;
		}
		/** create a new dst inode of symlink */
		Inode dstInode = getFreeInode();
		dstInode.init(Inode.TYPE_SYMLINK);
		SymLink symLink = new SymLink(dstInode, dstConj.name, this);
		symLink.init(srcPathString);
		dstConj.folder.createEntry(dstConj.name, dstInode); //add entry has done with linkCount
		return 0;
	}

	public String getPathString(Folder folder) {
		if (folder.inode.index == format.rootInodeIndex) {
			return "/";
		}
		else {
			FolderEntry entry = folder.getParentFolderEntry();
			Folder parentFolder = new Folder(entry.inode, this);
			parentFolder.load();
			return getPathStringRec(parentFolder) + folder.getName();
		}
	}

	public String getPathStringRec(Folder folder) {
		if (folder.inode.index == format.rootInodeIndex) {
			return "/";
		}
		else {
			FolderEntry entry = folder.getParentFolderEntry();
			Folder parentFolder = new Folder(entry.inode, this);
			parentFolder.load();
			return getPathStringRec(parentFolder) + folder.getName() + "/";
		}
	}

	public int getFreeSize()
	{
		//return 1024 * 10;
		//sectorFreeManager.print();
		//return sectorFreeManager.getFreeSize() * Disk.SectorSize;
		return 0;
	}

	public int getSwapFileSectors()
	{
		Tool.assertOccur(true, "a shabi call get swap file sector");
		return 0;
	}
}

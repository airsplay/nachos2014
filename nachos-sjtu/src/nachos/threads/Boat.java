package nachos.threads;

import nachos.ag.BoatGrader;
import java.util.LinkedList;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		System.out.println(adults + " " + children);
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.

		
		oChild = children;
		oAdult = adults;

		LinkedList<KThread> threads = new LinkedList<KThread>();
		for (int i = 1; i <= adults; i ++) {
			KThread t1 = new KThread(new Runnable() {
				public void run() {
					AdultItinerary();
				}
			});
			t1.setName("Adult " + i);
			t1.fork();
			threads.add(t1);
		}
		
		for (int i = 1; i <= children; i ++) {
			KThread t2 = new KThread(new Runnable() {
				public void run() {
					ChildItinerary();
				}
			});
			t2.setName("Child " + i);
			t2.fork();
			threads.add(t2);
		}
		for (KThread i : threads) {
			i.join();
		}

	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		while (true) {
		boatLock.acquire();
		if (oChild > 1) {
			oAdultCond.sleep();
			//oChildCond.wake();
		}
		else {
			if (boatLoad == 0) {
				bg.AdultRowToMolokai();
				oAdult--;
				mChildCond.wake();
				boatLock.release();
				return;
			}
			else {
				oAdultCond.sleep();
			}
		}
		boatLock.release();
		}
	}

	static void ChildItinerary() {
		while (true) {
			boatLock.acquire();
			if (oChild > 1){
				if (boatLoad == 0) {
					boatLoad ++;
					
					oChild --;
					mChild ++;
					bg.ChildRowToMolokai();
					
					oChildCond.wake();
					mChildCond.sleep();
					if (finish) {
						boatLock.release();
						return;
					}
					else {
						bg.ChildRowToOahu();
						oChild++;
						mChild--;
						oChildCond.wake();
						oChildCond.sleep();
					}
				}
				else if (boatLoad == 1) {
					bg.ChildRideToMolokai();
					bg.ChildRowToOahu();
					boatLoad = 0;
					oChildCond.wake();
				}
				else {
					oChildCond.sleep();
				}
			}
			else {
				if (boatLoad == 1) {
					if (oAdult == 0) {
						bg.ChildRideToMolokai();
						finish = true;
						mChildCond.wakeAll();
						boatLock.release();
						return;
					}
					else {
						bg.ChildRideToMolokai();
						bg.ChildRowToOahu();
						boatLoad = 0;
						oAdultCond.wake();
					}
				}
				else {
					oChildCond.sleep();
				}
				
			}
			boatLock.release();
		}
	}


	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

	public static int oChild = 0;
	public static int mChild = 0;
	public static int oAdult = 0;
	//public static int mAdult = 0;
	private static Lock boatLock = new Lock();
	private static Condition oChildCond = new Condition(boatLock);
	private static Condition oAdultCond = new Condition(boatLock);
	private static Condition mChildCond = new Condition(boatLock);
	private static int boatLoad = 0;
	private static boolean finish = false;
}

package nachos.filesys;

import java.util.LinkedList;
import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * FreeManager is a class to manage free things in fileSystem, belongs to meta data.
 * all meta data can be allocated in one sector(the basic assuption) .
 * @author airsplay
 */
/**
 * how to use:
 *	init allocate to the disk for the first time
 *	load loadfrom disk
 *	allocate-free
 *	
 *	save is free to call, just for compeleteness
 */
public class FreeManager
{
	public FreeManager(int savedSector, int size, RealFileSystem fs) {
		this.savedSector = savedSector;
		this.size = size;
		fileSystem = fs;
		//isFree = new boolean[size];
		buffer = new byte[Disk.SectorSize];
		freeList = new LinkedList<Integer>();
	}

	/** get the number of free elements in this mananger */
	public int getFreeSize() {
		return freeList.size();
	}

	public boolean isFree(int index) {
		return getBit(index);
	}

	/** the robber steal this index without freeList */
	public void occupy(int index) {
		changeBit(index, false);
		freeList.remove(new Integer(index));
	}

	public int allocate() {
		Integer sector = freeList.pollFirst();
		Tool.assertOccur(sector == null, "no free element");
		setBit(sector, false);
		return sector;
	}
	public void free(int index) {
		setBit(index, true);
		freeList.add(index);
	}

	public void print() {
		for (int i = 0; i < size; i++) {
			if (!getBit(i)) {
				System.out.println("not free" + i);
			}
		}
	}
	public void load() {
		fileSystem.readSector(savedSector, 0, Disk.SectorSize, buffer, 0);
		for (int i = 0; i < size; i ++) {
			if (getBit(i)) {
				freeList.add(i);
			}
		}
	}

	public void init() {
		for (int i = 0; i < size; i ++) {
			freeList.add(i);
		}
		for (int i = 0; i < buffer.length; i++) {
			buffer[i] = 0;
		}
		save();
	}

	public void save() {
		fileSystem.writeSector(savedSector, 0, Disk.SectorSize, buffer, 0);
	}

	public boolean getBit(int i) {
		int bytePos = i >> 3;
		int byteOffset = i & 7;
		return (buffer[bytePos] & (1 << byteOffset)) == 0;//if free, the bit is 0
	}
	
	public void changeBit(int i, boolean b) {
		int bytePos = i >> 3;
		int byteOffset = i & 7;
		if (b) //free :set 0
			buffer[bytePos] &= (~(1 << byteOffset) & ((1 << 8) - 1));
		else
			buffer[bytePos] |= 1 << byteOffset;
		//fileSystem.writeSector(sector, 0, Disk.SectorSize, buffer, 0);
	}
	
	public void setBit(int i, boolean b) {
		int bytePos = i >> 3;
		int byteOffset = i & 7;
		if (b) //free :set 0
			buffer[bytePos] &= (~(1 << byteOffset) & ((1 << 8) - 1));
		else
			buffer[bytePos] |= 1 << byteOffset;
		save();
		//fileSystem.writeSector(sector, 0, Disk.SectorSize, buffer, 0);
	}

	private LinkedList<Integer> freeList;
	//boolean []isFree;

	int savedSector;
	int size;
	byte []buffer;
	RealFileSystem fileSystem;
}

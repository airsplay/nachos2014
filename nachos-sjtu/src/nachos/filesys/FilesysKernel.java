package nachos.filesys;

import nachos.machine.Config;
import nachos.vm.VMKernel;

/**
 * A kernel with file system support.
 * 
 * @author starforever
 */
public class FilesysKernel extends VMKernel
{
	public static final char DEBUG_FLAG = 'f';

	public static RealFileSystem realFileSystem;

	public String folderName;
	public FilesysKernel ()
	{
		super();
	}

	public void initialize (String[] args)
	{
			super.initialize(args);
			boolean format = Config.getBoolean("FilesysKernel.format");
			fileSystem = realFileSystem = new RealFileSystem();
			realFileSystem.init(format);

	}

	@Override
	public File openFile (String name, boolean create) {
		return realFileSystem.open(realFileSystem.getRootFolder(), name ,create);
	}

	@Override
	public boolean removeFile (String name) {
		return realFileSystem.remove(realFileSystem.getRootFolder(), name);
	}
	
	public void selfTest ()
	{
		super.selfTest();
	}

//	public void handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
//		switch (syscall) {
//			case syscallMkDir:
//			case syscallRmDir:
//			case syscallChDir:
//			case syscallGetCwd:
//			case syscallReadDir:
//			case syscallLink:
//			case syscallSymlink:
//			default:
//				super.handleSyscall(syscall, a0, a1, a2, a3);
//		}
//	}
//
	public void terminate ()
	{
		realFileSystem.finish();
		super.terminate();
	}

	public final static int syscallMkDir = 14;
	public final static int syscallRmDir = 15;
	public final static int syscallChDir = 16;
	public final static int syscallGetCwd = 17;
	public final static int syscallReaddir = 18;
	public final static int syscallLink = 20;
	public final static int syscallSymlink = 21;

}

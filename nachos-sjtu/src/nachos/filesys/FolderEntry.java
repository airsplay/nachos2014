package nachos.filesys;

/** 
 * FolderEntry contains information used by Folder to map from filename to address of the file
 * 
 * @author starforever
 * */
class FolderEntry
{
	String name;

	Inode inode;

	public FolderEntry(String name, Inode inode) {
		this.name = name;
		this.inode = inode;
	}
}

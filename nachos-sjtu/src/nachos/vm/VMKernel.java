package nachos.vm;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;

import nachos.threads.Lock;
import nachos.threads.ThreadedKernel;
import nachos.userprog.UserKernel;
import nachos.machine.*;
/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
				super();
				

	}

	/**
	 * Initialize this kernel.
	 */
	public void initialize(String[] args) {
		super.initialize(args);
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		if (pageManager == null) pageManager = new PageManager();
		super.run();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		pageManager.kernelTerminate();
		super.terminate();
	}

	private static final char dbgVM = 'v';
	public class PageManager {
		public PageManager() {
			tlbManager = new TLBManager();
			pageTable = new PageTable();
			swapManager = new SwapManager();
			thumbManager = new ThumbManager();
		}
		public TranslationEntry getTranslationEntry(int pid, int localVpn) { //if can't find, then swapin it
			TranslationEntry entry = pageTable.getTranslationEntry(getUnicode(pid, localVpn));
			if (entry == null) { //no such entry in pageTable
				entry = handlePageFault(pid, localVpn);
				assertOccur(entry.dirty, "the newly load entry is dirty");
			}
			
			return entry;
		}
		public boolean handleTLBMiss(int pid, int localVpn) {
			//int unicode = getUnicode(pid, localVpn);
			TranslationEntry entry = getTranslationEntry(pid, localVpn);//pageTable.getTranslationEntry(unicode);

			//PageFault handle correct || Page hit
			entry = tlbManager.put(entry); //put the new entry into tlb, the return value is the old entry
			if (entry != null && entry.valid) //if valid, it belongs to this pid
				pageTable.refresh(getUnicode(pid, entry.vpn), entry); //refresh the TLB entry. only updata dirty & used
			return true;
		}
		public void dirty(int pid, int localVpn) {
			pageTable.setDirty(getUnicode(pid, localVpn), true);
		}
		public void use(int pid, int localVpn) {
			pageTable.setUsed(getUnicode(pid, localVpn), true);
		}
		public void PR(String s) {
			//System.out.println(s);
		}
		public void PF(String s) {
			//System.out.println(s);
		}
		public void assertOccur(boolean b, String s) {
			if (b) {
				System.out.println("CRITICAL ERROR:"+s);
			}
		}
		/**return the entry of new page
		 */
		public TranslationEntry handlePageFault(int pid, int localVpn) {
			int ppn = pageTable.getNextPpn(); //The next free memory (without remove if exist)
			int oldUnicode = pageTable.getUnicodeFromPpn(ppn); //unicode occupy this memory, not eaxactly this pid !!!!!
			if (oldUnicode == -1) { //ppn is a free memory //check must set free memory to -1
			}
			else {
				TranslationEntry entry = pageTable.getTranslationEntry(oldUnicode); 
				//if interupt here, then entry is not dirty, tlbEntry is not dirty too.
				TranslationEntry tlbEntry = null;
				if (getPidFromUnicode(oldUnicode) == pid) //only the old Unicode belongs to this pid, we check the tlb
					tlbEntry = tlbManager.remove(entry); //remove TLB block which reference this entry (with no put back) 
				//to avoid interupt, so load agian
				entry = pageTable.remove(oldUnicode); //remove from pageTable and set memory to be writable
				if (tlbEntry != null && entry != null) {
					assertOccur(tlbEntry.vpn != entry.vpn, "the vpn doesn't match");
					assertOccur(tlbEntry.ppn != entry.ppn, "the ppn doesn't match"+tlbEntry.ppn + " " + entry.ppn + "unicode:" + oldUnicode+"pid"+pid +"vpn"+localVpn);
				}
				if (entry.dirty) { //if an entry is readonly, it couldn't be dirty
					assertOccur(entry.readOnly, "a readOnly entry is dirty");
					swapManager.write(oldUnicode, ppn); //write the old page back to disk
				}
				else if (tlbEntry != null && tlbEntry.valid && tlbEntry.dirty) {
					assertOccur(tlbEntry.readOnly, "a readOnly entry is dirty");
					swapManager.write(oldUnicode, ppn); //write the old page back to disk
				}
			}

			int unicode = getUnicode(pid, localVpn);// put the unicode into ppn
			TranslationEntry newEntry;
			if (thumbManager.findPage(unicode)) { //Thumb hit
				newEntry = thumbManager.loadPage(unicode, ppn);
				assertOccur(newEntry.ppn != ppn, "the thumbMananger don't handle the ppn correctly");
				if (thumbManager.isDynamic(unicode)) { //dynamic means under the control of OS, and destroyed when process terminate
					assertOccur(newEntry.readOnly, "the thumbManager judge a readonly page as dynamic");
					thumbManager.remove(unicode);
					swapManager.allocate(unicode, newEntry);
				}
				else {
					assertOccur(!newEntry.readOnly, "the thumbManager judge a readable page as static");
				}
			}
			else newEntry = swapManager.read(unicode, ppn); 
			pageTable.put(unicode, newEntry);
			//PF("put page " + pid + " " + localVpn + " -----> " + ppn);
			//PF("pageFault " + pid + " " + localVpn + " finish");
			return newEntry;
		}
		public TranslationEntry[] contextSwitch(int pid) { //call when context switch 
			//System.out.println("context switch" + pid);
			TranslationEntry[] res = new TranslationEntry[Machine.processor().getTLBSize()];
			for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
				TranslationEntry entry = Machine.processor().readTLBEntry(i);
				res[i] = entry;
				if (entry == null) continue;
				if (entry.valid) { //the valid entry must hold by this pid
					pageTable.refresh(getUnicode(pid, entry.vpn), entry);
				}
				tlbManager.setInvalid(i);
			}
			return res;
			//tlbManager.setAllInvalid();
		}
		public void restoreTLB(int pid, TranslationEntry []savedTLB) {
			if (savedTLB == null) return;
			for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
				TranslationEntry entry = savedTLB[i];
				if (entry != null && entry.valid && pageTable.containsPage(getUnicode(pid, entry.vpn)))
					tlbManager.set(i, entry);	
			}
		}
		public void processFinish(int pid, int numPages) { //call when process finish 
			tlbManager.setAllInvalid();
			for (int vpn = 0; vpn < numPages; vpn ++) {
				int unicode = getUnicode(pid, vpn);
				pageTable.free(unicode); //attention: this free cost much
				swapManager.free(unicode); 
			}
		}
		public void kernelTerminate() { //call when kernel terminate 
			pageTable.clear();
			tlbManager.clear();
			swapManager.clear();
		}
		
		public class PageTable {
			
			public PageTable() {
				inversePageTable = new int[Machine.processor().getNumPhysPages()];
				globalPageTable = new HashMap<Integer, TranslationEntry>();
				ppnQueue = new LinkedList<Integer>();
				for (int ppn = 0; ppn < Machine.processor().getNumPhysPages(); ppn++) {
					ppnQueue.push(ppn);
					inversePageTable[ppn] = -1;
				}
			}
			public void refresh(int unicode, TranslationEntry entry) {
				setDirty(unicode, entry.dirty);
				setUsed(unicode, entry.used);
			}
			public void setDirty(int unicode, boolean dirty) {
				if (dirty) {
					TranslationEntry entry = globalPageTable.get(unicode);
					assertOccur(entry == null, "try to set a page dirty while it is not in pageTable");
					entry.dirty = true;
					globalPageTable.put(unicode, entry);
				}
			}
			public void setUsed(int unicode, boolean used) {
				if (used) {
					TranslationEntry entry = globalPageTable.get(unicode);
					assertOccur(entry == null, "try to set a page used while it is not in pageTable unicode:" + unicode);
					entry.used = used;
					globalPageTable.put(unicode, entry);
				}
			}
			
			public void put(int unicode, TranslationEntry entry) {
				ppnQueue.addLast(entry.ppn); //use this memory at last
				inversePageTable[entry.ppn] = unicode;
				globalPageTable.put(unicode, new TranslationEntry(entry));
			}
			public boolean containsPage(int unicode) {
				return globalPageTable.containsKey(unicode);
			}
			public void free(int unicode) {//remove(unicode) & push memory into ppnQueue
				TranslationEntry entry = globalPageTable.remove(unicode); //the entry which will be freed
				if (entry != null) {
					inversePageTable[entry.ppn] = -1;
					ppnQueue.addFirst(entry.ppn); //prefer use free memory 
				}
			}
			public TranslationEntry remove(int unicode) {//the memory will be occupied by another process, don't free it 
				TranslationEntry entry = globalPageTable.remove(unicode);
				assertOccur(entry == null, "try to remove a entry that is not in PageTable");
				inversePageTable[entry.ppn] = -1;
				return entry;
			}
			public TranslationEntry getTranslationEntry(int unicode) {
				return globalPageTable.get(unicode);
			}
			public int getNextPpn() {
				assertOccur(ppnQueue.isEmpty(), "no more ppn while acquiring the next ppn");
				int ppn = ppnQueue.pollFirst();
				return ppn;
			}
			public int getUnicodeFromPpn(int ppn) {
				return inversePageTable[ppn];
			}
			public void clear() {

			}
			
			Deque<Integer> ppnQueue;
			HashMap<Integer, TranslationEntry> globalPageTable;
			int []inversePageTable; //inverseGlobalPageTable -> globalPageTable -> TranslationEntry
		}
		public class TLBManager {
			public TLBManager() {
				r = new Random();
			}
			public void printStatus() {
				for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
					TranslationEntry entry = Machine.processor().readTLBEntry(i);
					PF(i+":"+entry.vpn+ " " + entry.ppn + " "+ entry.valid );
				}
			}
			public TranslationEntry remove(TranslationEntry entry) {
				for (int i = 0; i<Machine.processor().getTLBSize(); i++) {
					TranslationEntry tlbEntry = Machine.processor().readTLBEntry(i);
					if (tlbEntry.valid && tlbEntry.vpn == entry.vpn) { //because of context switch, the pid in TLB is the current
						setInvalid(i);
						return tlbEntry;
					}
				}
				return null;
			}
			//public TranslationEntry fetch(int index) {
			//	TranslationEntry entry = Machine.processor().readTLBEntry(index);

			//	return entry;
			//}
			public int nextTLBIndex() {
				int index;
				for (index = 0; index < Machine.processor().getTLBSize(); index++) {
					if (!Machine.processor().readTLBEntry(index).valid)
						break;
				}
				if (index == Machine.processor().getTLBSize()) index = r.nextInt(Machine.processor().getTLBSize());
				assertOccur(index >= Machine.processor().getTLBSize() || index < 0, "wrong next tlbindex");
				return index; //0 .. Machine.processor().getTLBSize(); 
			}
			public void setAllInvalid() {
				for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
					setInvalid(i);
				}
			}
			public void setInvalid(int index) {
				Machine.processor().writeTLBEntry(index, new TranslationEntry(-1, -1, false, false, false, false));	
			}
			public void set(int index, TranslationEntry entry) {
				Machine.processor().writeTLBEntry(index, entry); //check new TranslationEntry
			}
			public TranslationEntry put(TranslationEntry entry) {
				int index = nextTLBIndex();
				TranslationEntry returnEntry = Machine.processor().readTLBEntry(index);
				Machine.processor().writeTLBEntry(index, new TranslationEntry(entry));
				return returnEntry;
			}
			public void clear() {
				
			}
			Random r;
		}

		public int getUnicode(int pid, int localVpn) {
			return processUnicodeStart.get(pid) + localVpn;
		}
		public int getPidFromUnicode(int unicode) {
			return unicode2pid.get(unicode);
		}
		HashMap<Integer,Integer> unicode2pid = new HashMap<Integer, Integer>();
		public void extendUnicode(int pid) {
			if (processUnicodeStart.get(pid) == null) {
				processUnicodeStart.put(pid, unicodeRange);
				//System.out.println("unicode start of pid" + pid + "is " + unicodeRange);
			}
			unicode2pid.put(unicodeRange, pid); //a very ugly approach 
			assertOccur(processUnicodeStart.containsKey(pid+1), "unsorted unicode allocation");
			//PR("fuck extendUnicode pid:"+ pid +" unicodeRange:"+unicodeRange);
			unicodeRange ++;
		}

		public void createSectionPage(int pid, int vpn, CoffSection section, int sectionIndex) {
			extendUnicode(pid);
			thumbManager.allocateSectionPage(getUnicode(pid, vpn), vpn, section, sectionIndex);
		}
		public void createStackPage(int pid, int vpn) {
			extendUnicode(pid);
			thumbManager.allocateStackPage(getUnicode(pid, vpn), vpn);
		}
		public void createArgumentPage(int pid, int vpn) {
			extendUnicode(pid);
			thumbManager.allocateArgumentPage(getUnicode(pid, vpn), vpn);
		}
		public void printStatus() {
			tlbManager.printStatus();
		}
		public class ThumbManager{
			public ThumbManager() {

			}
			public void allocateSectionPage(int unicode, int vpn, CoffSection section, int sectionIndex) {
				assertOccur(thumbTable.containsKey(unicode), "already has this thumb, maybe a unicode collision");
				thumbTable.put(unicode, new SectionThumb(vpn, section, sectionIndex));
			}
			public void allocateStackPage(int unicode, int vpn) {
				assertOccur(thumbTable.containsKey(unicode), "already has this thumb, maybe a unicode collision");
				thumbTable.put(unicode, new StackThumb(vpn));
			}
			public void allocateArgumentPage(int unicode, int vpn) {
				assertOccur(thumbTable.containsKey(unicode), "already has this thumb, maybe a unicode collision");
				thumbTable.put(unicode, new ArgumentThumb(vpn));
			}
			public boolean findPage(int unicode) {
				return thumbTable.containsKey(unicode);
			}
			public TranslationEntry loadPage(int unicode, int ppn) {
				return thumbTable.get(unicode).load(ppn); 
			}
			public void remove(int unicode) {
				thumbTable.remove(unicode);
			}
			public boolean isDynamic(int unicode) {
				return thumbTable.get(unicode).isDynamic();
			}
			public class Thumb { //get idea from FengShi
				public Thumb() {

				}
				public TranslationEntry load(int ppn) {
					return null;
				}
				public boolean isDynamic() {
					return dynamic;
				}
				boolean dynamic;
			}
			public class SectionThumb extends Thumb{
				public SectionThumb(int _vpn, CoffSection _section, int _sectionIndex) {
					vpn = _vpn;
					section = _section;
					sectionIndex  = _sectionIndex;
					dynamic = false;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, section.isReadOnly(), false, false);
					section.loadPage(sectionIndex, ppn);
					PF("FUCK loadSection " + " vpn: "+vpn+" ppn: "+ppn);
					return entry;
				}
				public boolean isDynamic() {
					if (!section.isReadOnly()) {
						//System.out.println("MAYBE WRONG because of readonly");
					}
					return !section.isReadOnly();
				}
				private CoffSection section;
				private int sectionIndex;
				private int pid, vpn;
			}
			public class StackThumb extends Thumb{
				public StackThumb(int _vpn) {
					vpn = _vpn;
					dynamic = true;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, false, false, false);
					PF("FUCK loadStack " + " vpn: "+vpn+" ppn: "+ppn);
					//Memory needn't be filled
					return entry;
				}
				private int vpn;
			}
			public class ArgumentThumb extends Thumb {
				public ArgumentThumb(int _vpn) {
					vpn = _vpn;
					dynamic = true;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, false, false, false);
					PR("FUCK loadArgument " + " vpn: " + vpn + " ppn: " + ppn) ;
					return entry;
				}
				private int vpn;
			}
			Map<Integer, Thumb> thumbTable = new HashMap<Integer, Thumb>();
		}
		
		
		public class SwapManager {
			public SwapManager() {
				fileEnd = 0; //swapFile in use :[0, fileEnd)
				swapFile = openFile(swapFileName, true);
			}
			//public int fileEnd() {
			//	return fileEnd;
			//}
			public int allocateNewSwapPosition() {
				int returnPos = fileEnd;
				fileEnd = fileEnd + Machine.processor().pageSize;
				return returnPos;
			}
			public void allocate(int unicode, TranslationEntry entry) {
				int swapPosition;
				if (!freeSwapQueue.isEmpty()) {
					swapPosition = freeSwapQueue.poll();
				}
				else {
					swapPosition = allocateNewSwapPosition();
				}
				PF(unicode + "allocate at " + swapPosition);
				writePage(entry.ppn, swapPosition);
				swapTable.put(unicode, new SwapEntry(entry, swapPosition));
			}
			public void free(int unicode) {
				SwapEntry sEntry = swapTable.remove(unicode);
				if (sEntry != null) {
					freeSwapQueue.add(sEntry.swapPosition);
					PF("free swap " + sEntry.swapPosition);
				}
			}
			public int write(int unicode, int ppn) {
				SwapEntry sEntry = swapTable.get(unicode);
				assertOccur(sEntry == null, "write to a page not in swap. unicode:" + unicode);
				return writePage(ppn, sEntry.swapPosition);
			}
			public TranslationEntry read(int unicode, int ppn) {
				SwapEntry sEntry = swapTable.get(unicode);
				//PF(unicode + "read from" +sEntry.swapPosition);
				if (readPage(ppn, sEntry.swapPosition) != -1) {
					TranslationEntry returnEntry = new TranslationEntry(sEntry.entry);
					returnEntry.ppn = ppn;
					return returnEntry;
				}
				else return null;
			}
			public int writePage(int ppn, int pos) {
				int success = swapFile.write(pos, Machine.processor().getMemory(), ppn*Machine.processor().pageSize, Machine.processor().pageSize); //check must set free memory to -1
				assertOccur(success != Machine.processor().pageSize, "write page to swap unsuccessfully");
				if (success == Machine.processor().pageSize) return 0;
				else return -1;
			}
			public int readPage(int ppn, int pos) {
				int success = swapFile.read(pos, Machine.processor().getMemory(), ppn*Machine.processor().pageSize, Machine.processor().pageSize);
				assertOccur(success != Machine.processor().pageSize, "read page from swap unsuccessfully");
				if (success == Machine.processor().pageSize) return 0;
				else return -1;
			}
			public void clear() {
				swapFile.close();
				removeFile(swapFileName);
			}
			public class SwapEntry {
				TranslationEntry entry;
				int swapPosition;
				public SwapEntry (TranslationEntry _entry, int _swapPosition) {
					entry = _entry; swapPosition = _swapPosition;
				}
			}
			Queue<Integer> freeSwapQueue = new LinkedList<Integer>();
			OpenFile swapFile;
			HashMap<Integer, SwapEntry> swapTable = new HashMap<Integer, SwapEntry>();
			private static final String swapFileName = "SWAP";
			int fileEnd;
		}
		
		int unicodeRange = 0;
		TLBManager tlbManager;
		PageTable pageTable;
		SwapManager swapManager;
		ThumbManager thumbManager;
		Map<Integer, Integer> processUnicodeStart = new HashMap<Integer, Integer> ();
		
	}
	public static PageManager pageManager;
	public static PageManager pageManager() {
		return pageManager;
	}
}

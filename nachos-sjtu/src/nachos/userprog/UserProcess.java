package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.vm.VMKernel;

import java.io.EOFException;
import java.util.Vector;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		
		fdTable = new OpenFile[preserveFdNum + maxUserFdNum];
		fdTable[0] = UserKernel.console.openForReading();//stdin
		fdTable[1] = UserKernel.console.openForWriting();
		//fdTable[2] = UserKernel.console.openForWriting();
		for (int i = preserveFdNum; i < fdTable.length; i++){
			fdTable[i] = null;
		}
		
		

		
		newRunningProcess(this);
		
	}
	public void PR(int i) {
		//System.out.println(i);
	}
	public void newRunningProcess(UserProcess u) {
		lockPid.acquire();
		
		pid = ++pidCounter;
		//PR("nowPidcounter while new" + pidCounter);
		//PR("nowPidTableSize while new " + pidTable.size());
		//pidTable.
		
		if (pidTable.size() == 0) pidTable.add(null);
		pidTable.add(u);
		
		lockPid.release();
	}
	public void addPid() {
		lockPid.acquire();
		processRun++; 
		PR("process run " + pid + " all process " + processRun);
		lockPid.release();
	}
	public void deletePid() {
		lockPid.acquire();
		processRun--;
		lockPid.release();
	}
	public UserProcess getProcess(int pid) {
		lockPid.acquire();
		
		UserProcess process;
		//PR("requirePid " + pid);
		//PR("PidCounter " + pidCounter);
		//PR("pidLength " + pidTable.size());
		if (pid > pidCounter) process = null;
		else process =  pidTable.get(pid);
		
		lockPid.release();
		
		return process;
	}
	public void PR(String s) {
		//System.out.println(s);
	}
	public KThread getThread(int pid) {
		lockPid.acquire();
		
		KThread thread;
		if (pid >= pidCounter) thread = null;
		UserProcess process = pidTable.get(pid);
		if (process == null) thread = null;
		else if (process.thread == null) thread = null;
		else thread =  process.thread;

		lockPid.release();
		return thread;
	}
	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		UserProcess process = (UserProcess) Lib.constructObject(Machine.getProcessClassName());
		return process;
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		
		if (!load(name, args))
			return false;
		addPid();
		
		//lockPid.acquire();
		thread = new UThread(this);
		thread.setName(name).fork();//check maybe wrong. because fork!
		//lockPid.release();
		
		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}
	public Integer readVirtualMemoryInt(int vaddr) {
		byte[] bytes = new byte[4];
		int length = readVirtualMemory(vaddr, bytes);
		if (length == -1) {
			return null;
		}
		else if (length < 4) {
			sendError("Virtual memory can't read a int while reading memory");
			return null;
		}
		else {
			return new Integer(Lib.bytesToInt(bytes, 0));
		}

	}
	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);
		
		//PR("vaddr:"+ vaddr + "numBytes:"+numBytes);
		if (vaddr < 0 || vaddr >= numBytes)
			return 0;
		int successRead = 0;
		for (int i = 0; i < Math.min(length, numBytes - vaddr); i ++) {
			Byte b = readVirtualByte(vaddr + i);
			if (b == null) {
				data[offset+i] = 0;
				return successRead;
			}
			else {
				data[offset + i] = readVirtualByte(vaddr + i);
				successRead ++;
			}
		}
		return successRead;
	}

	public int writeVirtualMemoryString(int vaddr, String string) {
		byte[] buffer = (string+"\0").getBytes();
		return writeVirtualMemory(vaddr, buffer);
	}
	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);
		
		if (vaddr < 0 || vaddr >= numBytes)
			return 0;
		int successWrite = 0;
		for (int i = 0; i < Math.min(length, numBytes - vaddr); i ++) {
			if (writeVirtualByte(vaddr + i, data[offset + i])) {
				successWrite++;
			}
			else {
				return successWrite;
			}
		}
		return successWrite;
	}
	
	public void createPageTable() {
		pageTable = new TranslationEntry[numPages];
		for (int i = 0; i < numPages; i++)
			pageTable[i] = new TranslationEntry(i, i, true, false, false, false);
	}
	
	protected  boolean acquirePageTable() {
		createPageTable();
		for (int i = 0; i < numPages; i++) {
			pageTable[i].ppn = acquirePage();
			if (pageTable[i].ppn == -1) {
				sendError("No more physical page");
				return false;
			}
		}
		numBytes = numPages * pageSize;
		return true;
	}
	public void releasePageTable() {
		for (int i = 0; i < numPages; i++) {
			releasePage(pageTable[i].ppn);
		}
	}
	public int acquirePage() {
		int page = UserKernel.memoryManager.acquirePage();
		if (page == -1) {
			sendError("No more memory");
			return -1;
	//		cleanUp();
		}
		else return page;
	}
	public void releasePage(int page) {
		UserKernel.memoryManager.releasePage(page);
	}
	public Byte readVirtualByte(int vAddr) {
		int pAddr = realize(vAddr);
		if (pAddr == -1) return null;
		return UserKernel.memoryManager.readByte(pAddr);
	}
	public boolean writeVirtualByte(int vAddr, byte data) {
		int vpn = vAddr / pageSize;
		if (isReadOnly(vpn)) {
			return false;
		}
		int pAddr = realize(vAddr);	
		if (pAddr == -1) return false;
		UserKernel.memoryManager.writeByte(pAddr, data); 
		return true;
	}
	protected boolean isReadOnly(int vpn) {
		return pageTable[vpn].readOnly;
	}
	public int getVpn(int vAddr) {
		int vpn = vAddr / pageSize;
		if ((vpn >= numPages) || (vpn < 0)){
			sendError("Uncorrect vpn");
			return -1;
		}
		else {
			return vpn;
		}
	}
	protected int realize(int vAddr) { 
		PR("old realizing");

		int vpn = vAddr / pageSize;
		if (vpn >= numPages) {//the actual page is 0..numPages-1. For each page range is page*pagesize ~> page*pagesize + pagesize -1
			sendError("the virtual page execeeded");
			//cleanUp();
			return -1;
		}
		return pageTable[vpn].ppn * pageSize + vAddr % pageSize;
	}
	public int virtualize(int pAddr) {
		return 0;
	}
	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = openFile(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				//coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			//System.out.println(args[i]);
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			//coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		numPages++;
		// till now, complete count the numPages, allocate pageTable now;
		if (!acquirePageTable()) return false;
		
		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;
		System.out.println("argc" + argv.length);
		for (int i = 0; i < argv.length; i++) {
			System.out.println("argv:" + argv[i]);
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
				.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
				.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
				.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
	//	if (numPages > Machine.processor().getNumPhysPages()) {
	//		coff.close();
	//		Lib.debug(dbgProcess, "\tinsufficient physical memory");
	//		return false;
	//	}

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				pageTable[vpn].readOnly = section.isReadOnly();
				section.loadPage(i, pageTable[vpn].ppn);
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() { //todo
		releasePageTable();
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {
		if (!root) {
			sendError("A non-root process try to halt the system");
			return -1;
		}
		Machine.halt();

		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}

	public boolean removeFile(String name) {
		return ThreadedKernel.fileSystem.remove(name);
	}

	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
	 * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
	 *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
	 *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		//PR(((StubFileSystem)Machine.stubFileSystem()).getOpenCount());
		System.out.println("a system call occur :" + syscall);
		switch (syscall) {
			case syscallHalt:
				return handleHalt();
			case syscallCreate:

				return open(a0, true);
			case syscallOpen:
				return open(a0, false);
			case syscallClose:
				if (getFile(a0) == null) { //no such fd
					sendError("no such file descriptor");
					return -1;
				}
				fdTable[a0].close();
				fdTable[a0] = null;
				return 0;//check Error return -1;
			case syscallWrite:
				int fd = a0;
				int count = a2;
				if (getFile(a0) == null) {
					sendError("no such file descriptor");
					return -1;
				}
				byte[] buf = new byte[count];
				int length = readVirtualMemory(a1, buf, 0, count);
				if (length < count) {
					sendError("Error while read Memory");
					return -1;
				}

				int res =  fdTable[fd].write(buf, 0, count);
				
				//System.out.println("read finish");
				return res;
			case syscallRead:
				fd = a0;
				count = a2;
				if (getFile(a0) == null) {
					sendError("no such file descriptor");
					return -1;
				}
				buf = new byte[count];
				length = fdTable[fd].read(buf, 0, count);
				if (length == -1) {
					sendError("Error while reading file");
					return -1;
				}
				return writeVirtualMemory(a1, buf, 0, length);
			case syscallUnlink:
					String name = readVirtualMemoryString(a0, FileNameMaxLen);
					if (name == null) {
						sendError("Error name:maybe too long or invalid address");
						return -1;
					}
					if (removeFile(name)) {
						return 0;
					}
					else {
						sendError("Unsuccessful unlink the file" + name);
						return -1;
					}
			case syscallExec:
					PR("exec" + a0);
					VMKernel.pageManager().printStatus();
					String executeName = readVirtualMemoryString(a0, FileNameMaxLen);
					if (executeName == null) {
						sendError("Error while reading file name from memory:maybe too long or invalid address");
						return -1;
					}
					
					int argc = a1;
					//int argvStartPostion = a2;
					//String argv[argc];
					String []argv = new String[argc];
					for (int i = 0; i < argc; i++) {
						Integer argvPointer = readVirtualMemoryInt(a2 + 4*i);
						if (argvPointer == null) {
							sendError("Error while loading argv");
							return -1;
						}
						else {
							String argvString = readVirtualMemoryString(argvPointer, FileNameMaxLen);
							if (argvString == null) {
								sendError("Error while loading argvString");
								return -1;
							}
							else {
								argv[i] = argvString;
							}
						}
					}
					UserProcess process = UserProcess.newUserProcess();
					process.setPpid(this.pid);//the father of the process is this.
					if (!process.execute(executeName, argv)) {
						//process.cleanUp();
						return -1; 
					}
					//PR(""+process.getPid());
					return process.getPid();
			case syscallExit://todo no status return & check the haorder of finish & unload
					status = new Integer(a0);
					this.cleanUp();
					return 0; 
			case syscallJoin: 
					PR("join");
					int joinPid = a0;
					/*if (joinPid >= processRun) {
						sendError("Exceeding the max pid");
						return -1;
					}*/
					PR("process " + pid + " JOIN Pid " + joinPid);
					if (joinPid > pidCounter) {
						sendError("exceeding the maximum pid");
						return -1;
					}
					process = getProcess(joinPid);
					if (process == null) {
						sendError("child process terminate unsuccesfully");
						return -1;
						
					}
					if (process.thread == null) {
						sendError("unsetThread in join");
						return -1;
					}
					if (process.ppid != pid) {
						sendError("try to join a non-chlid process");
						return -1;
					}

					process.thread.join();

					PR(joinPid + " join Terminate");
					int statusPosition = a1;
					if (process.status == null) {
						PR("the child process terminate unsuccessfully");
						return 0; //the child process terminate uncesccefully
					}
					int status = process.status;
					byte [] data = new byte[4];
					Lib.bytesFromInt(data, 0, status);
					if (writeVirtualMemory(statusPosition, data) != 4) {
						sendError("Error while writing status in join");
						return -1;
					}
					PR("status "+status);

	//	PR(((StubFileSystem)Machine.stubFileSystem()).getOpenCount());
					//if (status == 0) return 1;
					//else return 0;
					return 1;
			default:
					System.out.println("CRITICAL ERROR:unhandled syscall");
					cleanUp();
					Lib.debug(dbgProcess, "Unknown syscall " + syscall);
					Lib.assertNotReached("Unknown system call!");
					
		}
		return 0;
	}

	public OpenFile openFile(String name, boolean create) {
		 return ThreadedKernel.fileSystem.open(name, create);
	}

	public OpenFile getFile(int a0) {
		if (a0 < 0 || a0 >= preserveFdNum + maxUserFdNum) {
			return null;
		}
		else {
			return fdTable[a0];
		}
	}
	public void setPpid(int pid) {
		lockPid.acquire();
		this.ppid = pid;
		lockPid.release();
	}
	public int getPpid() {
		lockPid.acquire();
		int tmpPpid =  this.ppid;
		lockPid.release();
		return tmpPpid;
	}
	public int open(int virtualAddr, boolean create) {
		String name = readVirtualMemoryString(virtualAddr, FileNameMaxLen);
		if (name == null) {
			sendError("The file name is too long");
			return -1;
		}
		OpenFile file = openFile(name, create);
		if (file == null) {
			sendError("no suchFile");
			return -1;
		}
		return allocateFd(file);
	}

	public int allocateFd(OpenFile file) {
		int i;
		for (i = preserveFdNum; i < fdTable.length; i++) {
			if (fdTable[i] == null) break;
		}
		if (i == fdTable.length) {
			sendError("Excceed the maximum of file limitation");
			return -1;
		}
		else {

			PR("allocate " + i);
			fdTable[i] = file;
			return i;
		}
	}

	public void sendError(String errorMsg) {
		System.out.println(errorMsg);
	}
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();
		VMKernel.pageManager().printStatus();
		switch (cause) {
			case Processor.exceptionSyscall:
				int result = handleSyscall(processor.readRegister(Processor.regV0),
						processor.readRegister(Processor.regA0), processor
						.readRegister(Processor.regA1), processor
						.readRegister(Processor.regA2), processor
						.readRegister(Processor.regA3));
				processor.writeRegister(Processor.regV0, result);
				processor.advancePC();
				break;

			default:
				System.out.println("CRITICAL ERROR:unhandled cause " + cause);
				cleanUp();
				Lib.debug(dbgProcess, "Unexpected exception: "
						+ Processor.exceptionNames[cause]);
				Lib.assertNotReached("Unexpected exception");
		}
	}

	public void setRoot() {
		root = true;
	}
	public void setPid(int newPid) {
		lockPid.acquire();
		pid = newPid;
		lockPid.release();
	}
	public int getPid() {
		lockPid.acquire();
		int tmpPid = pid;
		lockPid.release();
		return tmpPid;
	}
	public boolean noProcessAlive() {
		boolean tmp = false;
		lockPid.acquire();
		if (processRun == 0) tmp = true;
		lockPid.release();

		return tmp;
		
	}
	public int nowProcessAlive() {
		int tmp;
		lockPid.acquire();
		tmp = processRun;
		lockPid.release();
		
		return tmp;
	}
	//public void cleanUpAfterExec() {
	//	deletePid();
	//	cleanUp();
	//}
	protected void cleanUpWhileFailed() {
		cleanUp();	
	}
	protected void cleanUp() {

		deletePid();
		unloadSections();
		cleanFd();
		coff.close();
		PR("!process delete " + pid + " all process " + processRun);
		if (noProcessAlive()) 
			Kernel.kernel.terminate();
		KThread.finish();//upon finishing, this thread will not be schedule again
	}

	public void cleanFd() {
		for (int i = 0; i < preserveFdNum + maxUserFdNum; i ++) 
			if (fdTable[i] != null) {
				PR("clean " + i);
				fdTable[i].close();
				fdTable[i] = null;//???
			}
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;
	protected int numBytes;
	/** This process's map from file descriptor to OpenFile Object */
	protected OpenFile[] fdTable;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	protected int initialPC, initialSP;
	protected int argc, argv;
	protected int pid, ppid;
	protected UThread thread;
	protected boolean root = false;
	protected Integer status = null;
	

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final int maxUserFdNum = 18;//The max number of file that a user process can create
	private static final int preserveFdNum = 3;//Preserve fd, 0:standard input, 1:standard output, 2:standard Error
	public static final int FileNameMaxLen = 256;
	//private static final FileSystem fileSystem = ThreadedKernel.fileSystem;
	
	private static int pidCounter = 0;
	private static int processRun  = 0;
	private static Vector<UserProcess> pidTable = new Vector<UserProcess>();
	private static Lock lockPid = new Lock();

}

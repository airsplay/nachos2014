package nachos.threads;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		communicatorLock.acquire();
		//System.out.println("startspeak " + word);

		speakNum ++;
		buffer.add(new Integer(word));
		//while (listenNum == 0 || (flag)) {
		if(listenNum == 0){
			speakCond.sleep(); 
		}
		else {
			speakNum--;
			listenNum--;
			listenCond.wake();
		}
		//System.out.println("finishspeak " + word);
		communicatorLock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		int tmp;
		communicatorLock.acquire();
		listenNum ++;
		//System.out.println("startlisten " + listenNum);
		//while (speakNum == 0 || (!flag)) {
		if (speakNum == 0) {
 			listenCond.sleep();
 			tmp = buffer.remove();
		}
		else {
			speakNum--;
			listenNum--;
			tmp = buffer.remove();
			speakCond.wake();
		}
		communicatorLock.release();
		//System.out.println("finishlisten " + tmp);
		return tmp;
		
	}
	
	//private int buffer = 0;
	private Queue<Integer> buffer = new LinkedList<Integer>();
	//private Queue<Integer> buffer = new Queue<Integer>();
	private boolean flag = false;
	private int speakNum = 0;
	private int listenNum = 0;//all protected by communicatorLock
	
	private Lock communicatorLock = new Lock();
	private Condition listenCond = new Condition(communicatorLock);
	private Condition speakCond = new Condition(communicatorLock);
}

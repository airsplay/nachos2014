package nachos.vm;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;

import nachos.threads.Lock;
import nachos.threads.ThreadedKernel;
import nachos.userprog.UserKernel;
import nachos.machine.*;
/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
				super();
				

	}

	/**
	 * Initialize this kernel.
	 */
	public void initialize(String[] args) {
		super.initialize(args);
		if (pageManager == null) pageManager = new PageManager();
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		super.run();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		pageManager.kernelTerminate();
		super.terminate();
	}

	private static final char dbgVM = 'v';
	public class PageManager {
		public PageManager() {
			tlbManager = new TLBManager();
			pageTable = new PageTable();
			swapManager = new SwapManager();
			thumbManager = new ThumbManager();
		}
		public TranslationEntry getTranslationEntry(int pid, int localVpn) {
			TranslationEntry entry = pageTable.getTranslationEntry(getUnicode(pid, localVpn));
			if (entry == null) {
				entry = handlePageFault(pid, localVpn);
			}
			return entry;
		}
		public boolean handleTLBMiss(int pid, int localVpn) {
			//PF("TLBMiss " + pid + " " + localVpn);
			int unicode = getUnicode(pid, localVpn);
			TranslationEntry entry = pageTable.getTranslationEntry(unicode);
			if (entry == null || entry.valid == false) { //Page Fault occur
				entry = handlePageFault(pid, localVpn);
				return true;
			}
			//PF("vpn" + entry.vpn + "ppn" + entry.ppn);
			//PageFault handle correct || Page hit
			entry = tlbManager.put(entry); //fetch from tlb, will write back status to pageTable//check
			if (entry != null && entry.valid) //exact this pid's translation entry, o.w. omit it (transmit the tlb entry while context switch)
				pageTable.refresh(getUnicode(pid, entry.vpn), entry); //refresh the TLB entry. especially updata dirty & used
			//todo refresh each time use or logical//writeByte refresh
			//printStatus();
			PR("TLBMiss " + pid + " " + localVpn + " finish");
			return true;
		}
		public boolean dirty(int pid, int localVpn) {
			return pageTable.setDirty(getUnicode(pid, localVpn), true);
		}
		public boolean use(int pid, int localVpn) {
			return pageTable.setUsed(getUnicode(pid, localVpn), true);
		}
		public void PR(String s) {
			//System.out.println(s);
		}
		public void PF(String s) {
			System.out.println(s);
		}
		/**return the entry of new page
		 */
		public TranslationEntry handlePageFault(int pid, int localVpn) {
			PF("pageFault " + pid + " " + localVpn);
			int ppn = pageTable.getNextPpn(); //The next free memory (without remove if exist)
			int oldUnicode = pageTable.getUnicodeFromPpn(ppn); //unicode occupy this memory
			if (oldUnicode == -1) { //ppn is a free memory
			}
			else {
				TranslationEntry entry = pageTable.getTranslationEntry(oldUnicode); 
				tlbManager.remove(entry); //remove TLB block which reference this entry (with no put back) //check
				pageTable.remove(oldUnicode); //remove from pageTable and set memory to be writable
				if (entry.dirty) { //if an entry is readonly, it couldn't be dirty
					if (entry.readOnly) System.out.println("FUCKING WRONG");
					swapManager.write(oldUnicode, ppn); //write the old page back to disk
				}
			}

			int unicode = getUnicode(pid, localVpn);// put the unicode into ppn
			TranslationEntry newEntry;
			if (thumbManager.findPage(unicode)) { //Thumb hit
				newEntry = thumbManager.loadPage(unicode, ppn);
				if (thumbManager.isDynamic(unicode)) { //dynamic means under the control of OS, and destroyed when process terminate
					thumbManager.remove(unicode);
					swapManager.allocate(unicode, newEntry);
				}
			}
			else newEntry = swapManager.read(unicode, ppn); 
			pageTable.put(unicode, newEntry);
			PF("put page " + pid + " " + localVpn + " -----> " + ppn);
			PF("pageFault " + pid + " " + localVpn + " finish");
			return newEntry;
		}
		public void contextSwitch(int pid) { //call when context switch 
			//printStatus();
			for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
				//TranslationEntry entry = tlbManager.fetch(i);
				TranslationEntry entry = Machine.processor().readTLBEntry(i);
				if (entry == null) continue;
				if (!entry.valid) {
					//System.out.println("Fucking invalid on process " + pid + " tlb entry " + i);
					//entry.valid = true;
				}
				else {
					PR(" " + i +":" + "pid" + pid + "vpn" + entry.vpn + " unicode:" + getUnicode(pid, entry.vpn));
					pageTable.unblockedRefresh(getUnicode(pid, entry.vpn), entry);//updata the TLB entry before they all have been seted to invalid
				}
			}
			tlbManager.setAllInvalid();
		}
		public void processFinish(int pid, int numPages) { //call when process finish 
			tlbManager.setAllInvalid();
			for (int vpn = 0; vpn < numPages; vpn ++) {
				int unicode = getUnicode(pid, vpn);
				pageTable.free(unicode); //attention: this free cost much
				swapManager.free(unicode); 
			}
		}
		public void kernelTerminate() { //call when kernel terminate 
			pageTable.clear();
			tlbManager.clear();
			swapManager.clear();
		}
		
		public class PageTable {
			
			public PageTable() {
				inversePageTable = new int[Machine.processor().getNumPhysPages()];
				globalPageTable = new HashMap<Integer, TranslationEntry>();
				ppnQueue = new LinkedList<Integer>();
				for (int ppn = 0; ppn < Machine.processor().getNumPhysPages(); ppn++) {
					ppnQueue.push(ppn);
					inversePageTable[ppn] = -1;
				}
				//pageTableLock = new Lock();
			}
			public void refresh(int unicode, TranslationEntry entry) {
				pageLock.acquire();
				if (inversePageTable[entry.ppn] != unicode) {
					System.out.println("CRITICAL ERROR occur: the ppn not consistent");
					//Exception e = new Exception();
					//e.printStackTrace();
				}
				inversePageTable[entry.ppn] = unicode;
				if (!globalPageTable.containsKey(unicode)) {
					PR("critical Error occur:try to refresh a " + entry.vpn);
					Exception e = new Exception();
					e.printStackTrace();
				}
				//globalPageTable.put(unicode, entry);
				pageLock.release();
				setDirty(unicode, entry.dirty);
				setUsed(unicode, entry.used);
			}
			public void unblockedRefresh(int unicode, TranslationEntry entry) {
				if (inversePageTable[entry.ppn] != unicode) {
					System.out.println("CRITICAL ERROR occur: the ppn not consistent");
					//Exception e = new Exception();
					//e.printStackTrace();
				}
				inversePageTable[entry.ppn] = unicode;
				if (!globalPageTable.containsKey(unicode)) {
					PR("critical Error occur:try to refresh a " + entry.vpn);
					Exception e = new Exception();
					e.printStackTrace();
				}
				//globalPageTable.put(unicode, entry);
				unblockedSetDirty(unicode, entry.dirty);
				unblockedSetUsed(unicode, entry.used);
			}
			public boolean unblockedSetDirty(int unicode, boolean dirty) {
				if (dirty) {
					TranslationEntry entry = globalPageTable.get(unicode);
					if (entry == null) {
						return false;
					}
					entry.dirty = true;
					globalPageTable.put(unicode, entry);
				}
				return true;
			}
			public boolean unblockedSetUsed(int unicode, boolean used) {
				if (used) {
					TranslationEntry entry = globalPageTable.get(unicode);
					if (entry == null) {
						return false;
					}
					entry.used = used;
					globalPageTable.put(unicode, entry);
				}
				return true;
			}
			public boolean setDirty(int unicode, boolean dirty) {
				pageLock.acquire();
				if (dirty) {
					TranslationEntry entry = globalPageTable.get(unicode);
					if (entry == null) {
						pageLock.release();return false;
					}
					entry.dirty = true;
					globalPageTable.put(unicode, entry);
				}
				pageLock.release();
				return true;
			}
			public boolean setUsed(int unicode, boolean used) {
				pageLock.acquire();
				if (used) {
					TranslationEntry entry = globalPageTable.get(unicode);
					if (entry == null) {
						pageLock.release();return false;
					}
					entry.used = used;
					globalPageTable.put(unicode, entry);
				}
				pageLock.release();
				return true;
			}
			
			public void put(int unicode, TranslationEntry entry) {
				pageLock.acquire();
				ppnQueue.addLast(entry.ppn); //use this memory at last
				inversePageTable[entry.ppn] = unicode;
				globalPageTable.put(unicode, entry);
				pageLock.release();
			}
			public void free(int unicode) {//remove(unicode) & push memory into ppnQueue
				pageLock.acquire();
				TranslationEntry entry = globalPageTable.remove(unicode); //the entry which will be freed
				if (entry != null) {
					inversePageTable[entry.ppn] = -1;
					ppnQueue.addFirst(entry.ppn); //prefer use free memory 
				}
				pageLock.release();
			}
			public void remove(int unicode) {//the memory will be occupied by another process, don't free it 
				pageLock.acquire();
				TranslationEntry entry = globalPageTable.remove(unicode);
				if (entry != null) {
					inversePageTable[entry.ppn] = -1;
				}
				pageLock.release();
			}
			public TranslationEntry getTranslationEntry(int unicode) {
				return globalPageTable.get(unicode);
			}
//			public TranslationEntry getNextTranslationEntry() {
//				int ppn = ppnQueue.pollFirst();
//				TranslationEntry entry = globalPageTable.remove(inversePageTable[ppn]);
//				inversePageTable[ppn] = 0;
//				return entry;
//			}
			public int getNextPpn() {
				pageLock.acquire();	
				if (ppnQueue.isEmpty()) {
					PR("critical error occur:no more ppn");
				}
				int ppn = ppnQueue.pollFirst();
				//globalPageTable.remove(inversePageTable[ppn]);
				//inversePageTable[ppn] = -1;
				pageLock.release();
				return ppn;
				
			}
			public int getUnicodeFromPpn(int ppn) {
				return inversePageTable[ppn];
			}
			public void clear() {

			}
			
			Lock pageLock = new Lock();
			Deque<Integer> ppnQueue;
			HashMap<Integer, TranslationEntry> globalPageTable;
			int []inversePageTable; //inverseGlobalPageTable -> globalPageTable -> TranslationEntry
		}
		public class TLBManager {
			public TLBManager() {
				r = new Random();
				tlbLock = new Lock();
			}
			public void printStatus() {
				//tlbLock.acquire();
				for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
					TranslationEntry entry = Machine.processor().readTLBEntry(i);
					PF(i+":"+entry.vpn+ " " + entry.ppn + " "+ entry.valid );
				}
				//tlbLock.release();
			}
			public void remove(TranslationEntry entry) {
				tlbLock.acquire();
				for (int i = 0; i<Machine.processor().getTLBSize(); i++) {
					TranslationEntry tlbEntry = Machine.processor().readTLBEntry(i);
					if (tlbEntry.valid && tlbEntry.vpn == entry.vpn) { //because of context switch, the pid in TLB is the current
						Machine.processor().writeTLBEntry(i, new TranslationEntry(0, 0, false, false, false, false));
						//actually setInvalid. to prevent acquire lock twice
					}
				}
				tlbLock.release();
			}
			public TranslationEntry fetch(int index) {
				tlbLock.acquire();
				TranslationEntry entry = Machine.processor().readTLBEntry(index);
				tlbLock.release();

				return entry;
			}
			public int nextTLBIndex() {
				int index;
				for (index = 0; index < Machine.processor().getTLBSize(); index++) {
					if (!Machine.processor().readTLBEntry(index).valid)
						break;
				}
				if (index == Machine.processor().getTLBSize()) index = 0;//todo randomize
				return index; //0 .. Machine.processor().getTLBSize(); 
			}
			public void setAllInvalid() {
				for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
					setInvalid(i);
				}
			}
			public void setInvalid(int index) {
				//tlbLock.acquire();
				PR("set invalid " + index);
				Machine.processor().writeTLBEntry(index, new TranslationEntry(0, 0, false, false, false, false));	
				//tlbLock.release();
			}
			public TranslationEntry put(TranslationEntry entry) {
				
				tlbLock.acquire();
				int index = nextTLBIndex();
				TranslationEntry returnEntry = Machine.processor().readTLBEntry(index);
				Machine.processor().writeTLBEntry(index, entry);
				tlbLock.release();
				return returnEntry;
			}
			public void clear() {
				
			}
			Random r;
			Lock tlbLock;
		}

		public int getUnicode(int pid, int localVpn) {
			return processUnicodeStart.get(pid) + localVpn;
		}
//		public void flushIntoSwap(int pid, TranslationEntry[] userPageTable, int numPages) { //Used to move the initial page from memory to swap
//			processUnicodeStart.put(pid, unicodeRange);//the start position which assign to a particular process
//			unicodeRange = unicodeRange + numPages;
//			for (int localVpn = 0; localVpn < numPages; localVpn++) {
//				TranslationEntry entry = userPageTable[localVpn];
//				swapManager.allocate(getUnicode(pid, localVpn), entry);
//			}
//		}
		public void extendUnicode(int pid) {
			if (processUnicodeStart.get(pid) == null) {
				processUnicodeStart.put(pid, unicodeRange);
				System.out.println("unicode start of pid" + pid + "is " + unicodeRange);
			}
			PR("fuck extendUnicode pid:"+ pid +" unicodeRange:"+unicodeRange);
			unicodeRange ++;
		}
		public void createSectionPage(int pid, int vpn, CoffSection section, int sectionIndex) {
			extendUnicode(pid);
			thumbManager.allocateSectionPage(getUnicode(pid, vpn), vpn, section, sectionIndex);
		}
		public void createStackPage(int pid, int vpn) {
			extendUnicode(pid);
			thumbManager.allocateStackPage(getUnicode(pid, vpn), vpn);
		}
		public void createArgumentPage(int pid, int vpn) {
			extendUnicode(pid);
			thumbManager.allocateArgumentPage(getUnicode(pid, vpn), vpn);
		}
		public void printStatus() {
			tlbManager.printStatus();
		}
		public class ThumbManager{
			public ThumbManager() {

			}
			public void allocateSectionPage(int unicode, int vpn, CoffSection section, int sectionIndex) {
				if (thumbTable.containsKey(unicode)) System.out.println("critical error");
				thumbTable.put(unicode, new SectionThumb(vpn, section, sectionIndex));
			}
			public void allocateStackPage(int unicode, int vpn) {
				if (thumbTable.containsKey(unicode)) PR("FUCKING WRONG");
				thumbTable.put(unicode, new StackThumb(vpn));
			}
			public void allocateArgumentPage(int unicode, int vpn) {
				if (thumbTable.containsKey(unicode)) PR("FUCKING WRONG");
				thumbTable.put(unicode, new ArgumentThumb(vpn));
			}
			public boolean findPage(int unicode) {
				return thumbTable.containsKey(unicode);
			}
			public TranslationEntry loadPage(int unicode, int ppn) {
				return thumbTable.get(unicode).load(ppn); 
			}
			public void remove(int unicode) {
				thumbTable.remove(unicode);
			}
			public boolean isDynamic(int unicode) {
				return thumbTable.get(unicode).isDynamic();
			}
			public class Thumb { //get idea from FengShi
				public Thumb() {

				}
				public TranslationEntry load(int ppn) {
					return null;
				}
				public boolean isDynamic() {
					return dynamic;
				}
				boolean dynamic;
			}
			public class SectionThumb extends Thumb{
				public SectionThumb(int _vpn, CoffSection _section, int _sectionIndex) {
					vpn = _vpn;
					section = _section;
					sectionIndex  = _sectionIndex;
					dynamic = false;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, section.isReadOnly(), false, false);
					section.loadPage(sectionIndex, ppn);
					PF("FUCK loadSection " + " vpn: "+vpn+" ppn: "+ppn);
					return entry;
				}
				public boolean isDynamic() {
					if (!section.isReadOnly()) {
						System.out.println("MAYBE WRONG because of readonly");
					}
					return !section.isReadOnly();
				}
				private CoffSection section;
				private int sectionIndex;
				private int pid, vpn;
			}
			public class StackThumb extends Thumb{
				public StackThumb(int _vpn) {
					vpn = _vpn;
					dynamic = true;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, false, false, false);
					PF("FUCK loadStack " + " vpn: "+vpn+" ppn: "+ppn);
					//Memory needn't be filled
					return entry;
				}
				private int vpn;
			}
			public class ArgumentThumb extends Thumb {
				public ArgumentThumb(int _vpn) {
					vpn = _vpn;
					dynamic = true;
				}
				public TranslationEntry load(int ppn) {
					TranslationEntry entry = new TranslationEntry(vpn, ppn, true, false, false, false);
					PR("FUCK loadArgument " + " vpn: " + vpn + " ppn: " + ppn) ;
					return entry;
				}
				private int vpn;
			}
			Map<Integer, Thumb> thumbTable = new HashMap<Integer, Thumb>();
		}
		
		public class SwapManager {
			public SwapManager() {
				fileEnd = 0; //swapFile in use :[0, fileEnd)
				swapFile = ThreadedKernel.fileSystem.open(swapFileName, true);
			}
			//public int fileEnd() {
			//	return fileEnd;
			//}
			public int allocateNewSwapPosition() {
				int returnPos = fileEnd;
				fileEnd = fileEnd + Machine.processor().pageSize;
				return returnPos;
			}
			public void allocate(int unicode, TranslationEntry entry) {
				int swapPosition;
				if (!freeSwapQueue.isEmpty()) {
					swapPosition = freeSwapQueue.poll();
				}
				else {
					swapPosition = allocateNewSwapPosition();
				}
				PF(unicode + "allocate at " + swapPosition);
				writePage(entry.ppn, swapPosition);
				swapTable.put(unicode, new SwapEntry(entry, swapPosition));
			}
			public void free(int unicode) {
				SwapEntry sEntry = swapTable.remove(unicode);
				if (sEntry != null) {
					freeSwapQueue.add(sEntry.swapPosition);
					PF("free swap " + sEntry.swapPosition);
				}
			}
			public int write(int unicode, int ppn) {
				SwapEntry sEntry = swapTable.get(unicode);
				PF(unicode + "write to" +sEntry.swapPosition);
				return writePage(ppn, sEntry.swapPosition);
			}
			public TranslationEntry read(int unicode, int ppn) {
				SwapEntry sEntry = swapTable.get(unicode);
				PF(unicode + "read from" +sEntry.swapPosition);
				if (readPage(ppn, sEntry.swapPosition) != -1) {
					TranslationEntry returnEntry = new TranslationEntry(sEntry.entry);
					returnEntry.ppn = ppn;
					return returnEntry;
				}
				else return null;
			}
			public int writePage(int ppn, int pos) {
				int success = swapFile.write(pos, Machine.processor().getMemory(), ppn*Machine.processor().pageSize, Machine.processor().pageSize);
				if (success == Machine.processor().pageSize) return 0;
				else return -1;
			}
			public int readPage(int ppn, int pos) {
				int success = swapFile.read(pos, Machine.processor().getMemory(), ppn*Machine.processor().pageSize, Machine.processor().pageSize);
				if (success == Machine.processor().pageSize) return 0;
				else return -1;
			}
			public void clear() {
				swapFile.close();
				ThreadedKernel.fileSystem.remove(swapFileName);
			}
			public class SwapEntry {
				TranslationEntry entry;
				int swapPosition;
				public SwapEntry (TranslationEntry _entry, int _swapPosition) {
					entry = _entry; swapPosition = _swapPosition;
				}
			}
			Queue<Integer> freeSwapQueue = new LinkedList<Integer>();
			OpenFile swapFile;
			HashMap<Integer, SwapEntry> swapTable = new HashMap<Integer, SwapEntry>();
			private static final String swapFileName = "SWAP";
			int fileEnd;
		}
		
		int unicodeRange = 0;
		TLBManager tlbManager;
		PageTable pageTable;
		SwapManager swapManager;
		ThumbManager thumbManager;
		Map<Integer, Integer> processUnicodeStart = new HashMap<Integer, Integer> ();
		
	}
	public static PageManager pageManager;
	public static PageManager pageManager() {
		return pageManager;
	}
}
